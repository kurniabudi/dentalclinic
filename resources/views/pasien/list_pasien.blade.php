@extends('layouts.app', ['active' => 'list_pasien'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Pasien</a></li>
			<li class="active">Daftar</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-2">
						<button id="btn-add" class="btn btn-default"><span class="icon-user-plus"></span> Tambah</button>
					</div>
					<div class="col-lg-2">
						<button id="btn-daftar" class="btn bg-pink-400"><span class="icon-file-text3"></span> Daftar</button>
					</div>
					
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>NO. RM</th>
									<th>NIK</th>
									<th>Nama</th>
									<th>TTL</th>
									<th>Alamat</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('regist.printPeriksa') }}" id="printNP" target="_blank"></a>
@endsection

@section('modal')
	@include('pasien._modal_add')
	@include('pasien._modal_daftar')
	@include('pasien._modal_list')
	@include('pasien._modal_edit')
@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$(window).on('load',function(){
		loading();
		table.clear();
		table.draw();
		$.unblockUI();
	});

	

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('pasien.getPasien') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'user_id', name: 'user_id'},
            {data: 'nik', name: 'nik'},
            {data: 'nama', name: 'nama'},
            {data: 'tempat_lahir', name: 'tempat_lahir'},
            {data: 'alamat', name: 'alamat'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
	});

	// modal_daftar
	$('#btn-daftar').click(function(){
		ajaxListPasien();
		$('#modal_daftar').modal('show');

	});

	$('#norm').on('change',function(event){
		event.preventDefault();
		
		if ($('#norm').val()=="") {
			$('#txrm').val("");
        	$('#txttl').val("");
        	$('#txgender').val("");
        	$('#txalamat').val("");
		}else{

			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax({
		        type: 'get',
		        url :  "{{ route('regist.ajaxDetPasRm') }}",
		        data : {norm:$('#norm').val()},
		        success: function(response) {
		        	var data = response.data;

		        	$('#txrm').val(data.user_id);
		        	$('#txttl').val(data.tempat_lahir+' '+data.tanggal_lahir);
		        	$('#txgender').val(data.gender);
		        	$('#txalamat').val(data.alamat+'  Kel. '+data.nama_kelurahan+'  Kec. '+data.nama_kecamatan);
		  
		        },
		        error: function(response) {
		           console.log(response);
		          
		        }
		    });
		}
	});

	$('#form-daftar').submit(function(e){
		e.preventDefault();

		if ($('#txrm').val()=="" || $('#txrm').val()==null) {
			alert(422,"Pasien belum dipilih ! ! !");

			return false;
		}

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-daftar').attr('action'),
	        data : {norm:$('#txrm').val(),poli:$('#poli').val(),dokt:$('#dokter').val()},
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var notif = response.data;
                alert(notif.status,notif.output);
                print(notif.idpr);
                $.unblockUI();
                $('#modal_daftar').modal('hide');
	         	
	        },
	        error: function(response) {
	           $.unblockUI();
		       alert(response.status,response.responseText);
	            
	        }
	    });
	});
	// modal_daftar

	//modal_add
	$('#btn-add').click(function(){
		$('#nik').val('');
		$('#nama').val('');
		$('#alamat').val('');
		$('#tmp_lhr').val('');
		$('#tgl_lhr').val('');
		$('#gender').val('LAKI-LAKI').trigger('change');
		$('#agama').val('ISLAM').trigger('change');
		$('#nohp').val('');
		$('#idkel').empty();
		$('#idkel').append('<option value="">--Pilih Kelurahan--</option>');
		ajaxSetKec();

		$('#modal_add').modal('show');
	});

	$('#form-add').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-add').attr('action'),
	        data : {name:$('#nama').val(),nik:$('#nik').val(),tmp_lhr:$('#tmp_lhr').val(),tgl_lhr:$('#tgl_lhr').val(),gender:$('#gender').val(),agama:$('#agama').val(),alamat:$('#alamat').val(),nohp:$('#nohp').val(),idkel:$('#idkel').val()},
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var notif = response.data;
                alert(notif.status,notif.output);
                $.unblockUI();

                $('#modal_add').modal('hide');
                table.clear();
                table.draw();
	         	
	        },
	        error: function(response) {
	           $.unblockUI();
		       alert(response.status,response.responseText);
	            
	        }
	    });
	});
	//modal_add

	// $('#modal_edit').on('shown.bs.modal',function(){
	// 	ajaxsetKecEd();
	// });
	
	$('#table-list').on('click','.editpasien',function(event){

		var id = $(this).data('id');

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  "{{ route('pasien.ajaxDetPasien') }}",
	        data : {id:id},
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	
	         	var data = response.data;

	         	$('#modal_edit .edid').val(data.user_id);
	         	$('#modal_edit .ednik').val(data.nik);
	         	$('#modal_edit .ednama').val(data.nama);
	         	$('#modal_edit .edtmp_lhr').val(data.tempat_lahir);
	         	$('#modal_edit .edtgl_lhr').val(data.tanggal_lahir);
	         	$('#modal_edit .edgender').val(data.gender).trigger('change');
	         	$('#modal_edit .edagama').val(data.agama).trigger('change');
	         	
	         	$('#modal_edit .ednohp').val(data.no_hp);
	         	ajaxsetKecEd(data.id_kec);
	         	// $('#edidkec').val(data.id_kec).trigger('change');
	         	$('#modal_edit .txedkel').val(data.id_kel);
	         	$('#modal_edit .edalamat').val(data.alamat);

	         	$('#modal_edit').modal('show');
	         	$.unblockUI();
	        },
	        error: function(response) {
	          	alert(422,response.responseText);
	          	$.unblockUI();
	            
	        }
	    });
	});
	
	$('#form-edit').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-edit').attr('action'),
	        data : {
	        	user_id:$('#edid').val(),
	        	nik:$('#ednik').val(),
	        	nama:$('#ednama').val(),
	        	alamat:$('#edalamat').val(),
	        	tanggal_lahir:$('#edtgl_lhr').val(),
	        	tempat_lahir:$('#edtmp_lhr').val(),
	        	nohp:$('#ednohp').val(),
	        	gender:$('#edgender').val(),
	        	agama:$('#edagama').val(),
	        	idkel:$('#txedkel').val()
	        },
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	
	         	var notif = response.data;
                alert(notif.status,notif.output);
                $.unblockUI();

                $('#modal_edit').modal('hide');
                table.clear();
                table.draw();
	         	
	        },
	        error: function(response) {
	          	alert(response.status,response.responseText);
	          	$.unblockUI();

	          	$('#modal_edit').modal('hide');
                table.clear();
                table.draw();
	            
	        }
	    });

	})
});


function ajaxSetKec(){
	
	$.ajax({
        type: 'get',
        url :  "{{ route('pasien.ajaxListKec') }}",
        success: function(response) {

         	var kec = response.kec;

         	$('#idkec').empty();
         	$('#idkec').append('<option value="">--Pilih Kecamatan</option>');
         	for (var i = 0; i < kec.length; i++) {

         		$('#idkec').append('<option value="'+kec[i]['id']+'">'+kec[i]['nama_kecamatan']+'</option>');
         	}
        },
        error: function(response) {
           console.log(response);
            
        }
    });
	
    
}

function ajaxListKel(e){
	var idkec = $('#idkec').val();
	$.ajax({
        type: 'get',
        url :  "{{ route('pasien.ajaxListKel') }}",
        data:{idkec:idkec},
        success: function(response) {
        
         	var kel = response.kel;

         	$('#idkel').empty();
         	$('#idkel').append('<option value="">--Pilih Kelurahan--</option>');
         	for (var i = 0; i < kel.length; i++) {
         		$('#idkel').append('<option value="'+kel[i]['id']+'">'+kel[i]['nama_kelurahan']+'</option>');
         	}
        },
        error: function(response) {
           console.log(response);
            
        }
    });
}

function ajaxListPasien(){
	$.ajax({
        type: 'get',
        url :  "{{route('regist.ajaxGetPasienPoli')}}",
        success: function(response) {
        	var pas = response.pasien;
         	$('#norm').empty();
         	$('#norm').append('<option value="">--Pilih Pasien--</option>');
         	for (var i = 0; i < pas.length; i++) {
         		

         		$('#norm').append('<option value="'+pas[i]['user_id']+'">'+pas[i]['nama']+'  ( '+pas[i]['nik']+' )</option>');
         	}

         	var pol = response.poli;
         	$('#poli').empty();
         	$('#poli').append('<option value="">--Pilih Poli--</option>');
         	for (var i = 0; i < pol.length; i++) {
         		

         		$('#poli').append('<option value="'+pol[i]['id']+'">'+pol[i]['nama_poli']+'</option>');
         	}
        },
        error: function(response) {
           console.log(response);
            
        }
    });
}

function print(id){
	var url = $('#printNP').attr('href')+'?id='+id;

	window.open(url);
}


function viewList(e){
	var id = e.getAttribute('data-id');
	
	var tableRm = $('#table-rm').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('pasien.ajaxRm') }}",
            data:{user_id:id}
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableRm.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'status', name: 'status'},
            {data: 'hasil', name: 'hasil'},
            {data: 'action', name: 'action'}
        ]
	});

	$('#modal_rm').modal('show');
}

function changeStat(e){
	var id = e.getAttribute('data-id');
	var val = $('#table-rm .status').val();

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url :  "{{ route('pasien.changeStatus') }}",
        data : {id:id,val:val},
        beforeSend:function(){
        	loading();
        },
        success: function(response) {
        	var notif = response.data;
            alert(notif.status,notif.output);
            $.unblockUI();
            window.location.reload();
            
           
         	
        },
        error: function(response) {
           $.unblockUI();
	       alert(response.status,response.responseText);
            
        }
    });
	
}


function ajaxsetKecEd(idkec){
	

	$.ajax({
        type: 'get',
        url :  "{{ route('pasien.ajaxListKec') }}",
        success: function(response) {
        
         	var kec = response.kec;

         	$('#modal_edit .edidkec').empty();
         	$('#modal_edit .edidkec').append('<option value="">--Pilih Kecamatan--</option>');
         	for (var i = 0; i < kec.length; i++) {
         		if (kec[i]['id']==idkec) {
         			var selc = 'selected';
         		}else{
         			var selc = '';
         		}
         		
         		$('#modal_edit .edidkec').append('<option value="'+kec[i]['id']+'">'+kec[i]['nama_kecamatan']+'</option>');
         	}

         	$('#modal_edit .edidkec').val(idkec).trigger('change');

        },
        error: function(response) {
           console.log(response);
            
        }
    });


}

function ajaxsetKelEd(){
	var idkec = $('#modal_edit .edidkec').val();
	var idkel = $('#modal_edit .txedkel').val();
	$.ajax({
        type: 'get',
        url :  "{{ route('pasien.ajaxListKel') }}",
        data:{idkec:idkec},
        success: function(response) {
        
         	var kel = response.kel;

         	$('#modal_edit .edidkel').empty();
         	$('#modal_edit .edidkel').append('<option value="">--Pilih Kelurahan--</option>');
         	for (var i = 0; i < kel.length; i++) {
         		
         		if (kel[i]['id']==idkel) {
         			var selc = 'selected';
         		}else{
         			var selc = '';
         		}

         		$('#modal_edit .edidkel').append('<option value="'+kel[i]['id']+'" '+selc+'>'+kel[i]['nama_kelurahan']+'</option>');
         	}

         	// $('#modal_edit .edidkel').val(idkec).trigger('change');

        },
        error: function(response) {
           console.log(response);
            
        }
    });
}

function getDokPol(e){
	var value = $('#modal_daftar .poli').val();

	

	$.ajax({
        type: 'get',
        url :  "{{ route('regist.getDok') }}",
        data:{idpoli:value},
        success: function(response) {
        
         	var dok = response.data;

         	$('#modal_daftar .dokter').empty();
         	$('#modal_daftar .dokter').append('<option value="">--Pilih Dokter--</option>');
         	for (var i = 0; i < dok.length; i++) {
         	

         		$('#modal_daftar .dokter').append('<option value="'+dok[i]['id']+'">'+dok[i]['nama_dokter']+'</option>');
         	}


        },
        error: function(response) {
           console.log(response);
            
        }
    });
}



</script>
@endsection