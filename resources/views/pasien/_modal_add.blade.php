
<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Pasien Baru</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('pasien.addPasien') }}" id="form-add">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">NIK</label>
							<input type="text" name="nik" id="nik" class="form-control" required="" style="text-transform: uppercase;">
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama</label>
							<input type="text" name="nama" id="nama" class="form-control" required="" style="text-transform: uppercase;">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Tempat Lahir</label>
							<input type="text" name="tmp_lhr" id="tmp_lhr" class="form-control" required="" style="text-transform: uppercase;">
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Tanggal Lahir</label>
							<input type="date" name="tgl_lhr" id="tgl_lhr" class="form-control" required="" style="text-transform: uppercase;">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Gender</label>
							<select class="form-control select" id="gender">
								<option value="LAKI-LAKI">LAKI-LAKI</option>
								<option value="PEREMPUAN">PEREMPUAN</option>
							</select>
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Agama</label>
							<select class="form-control select" id="agama">
								<option value="ISLAM">ISLAM</option>
								<option value="KRISTEN">PROTESTAN</option>
								<option value="KATOLIK">KATOLIK</option>
								<option value="HINDU">HINDU</option>
								<option value="BUDHA">BUDHA</option>
								<option value="KONGHUCU">KONGHUCU</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">No. Telp</label>
							<input type="text" name="nohp" id="nohp" class="form-control" required="" style="text-transform: uppercase;">
						</div>
						<div class="col-lg-3">
							<label class="display-block text-semibold">Kecamatan</label>
							<select id="idkec" class="form-control select" onchange="ajaxListKel(this);"></select>
						</div>
						<div class="col-lg-3">
							<label class="display-block text-semibold">Kelurahan</label>
							<select id="idkel" class="form-control select">
								<option value="">--Pilih Kelurahan--</option>
							</select>
						</div>
						<div class="col-lg-12">
							<label class="display-block text-semibold">Alamat</label>
								<input type="text" name="alamat" id="alamat" class="form-control" required="" style="text-transform: uppercase;">
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" id="btn-save" class="btn btn-primary"><span class="icon-address-book"></span> Simpan</button>
							<button type="button" id="btn-save" class="btn btn-warning" onclick="dispose();"><span class="icon-x"></span> Batal</button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
