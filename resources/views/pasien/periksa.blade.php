@extends('layouts.app', ['active' => 'periksa'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Pasien</a></li>
			<li class="active">Daftar</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-2">
						<button id="btn-add" class="btn btn-default"><span class="icon-user-plus"></span> Tambah</button>
					</div>
					<div class="col-lg-2">
						<button id="btn-daftar" class="btn bg-pink-400"><span class="icon-file-text3"></span> Daftar</button>
					</div>
					
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>NO. RM</th>
									<th>NIK</th>
									<th>Nama</th>
									<th>TTL</th>
									<th>Alamat</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection




@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$(window).on('load',function(){
		loading();
		table.clear();
		table.draw();
		$.unblockUI();
	});

	

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('pasien.getPasien') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'user_id', name: 'user_id'},
            {data: 'nik', name: 'nik'},
            {data: 'nama', name: 'nama'},
            {data: 'tempat_lahir', name: 'tempat_lahir'},
            {data: 'alamat', name: 'alamat'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
	});

	

	
	
});



</script>
@endsection