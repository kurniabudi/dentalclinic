@extends('layouts.app', ['active' => 'list_pasien'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Pasien</a></li>
			<li class="active">Daftar</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="user-list">
							<thead>
								<tr>
									<th>#</th>
									<th>NO. RM</th>
									<th>NIK</th>
									<th>Nama</th>
									<th>Gender</th>
									<th>TTL</th>
									<th>Alamat</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js')
<script type="text/javascript">
$(document).ready(function(){
	$(window).on('load',function(){
		
	});

	

	var table = $('#user-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "#"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'no_rm', name: 'no_rm'},
            {data: 'nik', name: 'nik'},
            {data: 'nama', name: 'nama'},
            {data: 'gender', name: 'gender'},
            {data: 'tempat_lahir', name: 'tempat_lahir'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
	});



	

	

	
});





</script>
@endsection