
<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Edit Pasien</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('pasien.editPasien') }}" id="form-edit">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">No. RM</label>
							<input type="text" name="edid" id="edid" class="form-control edid" readonly="">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">NIK</label>
							<input type="text" name="ednik" id="ednik" class="form-control ednik" required="">
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama</label>
							<input type="text" name="ednama" id="ednama" class="form-control ednama" required="" style="text-transform: uppercase;">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Tempat Lahir</label>
							<input type="text" name="edtmp_lhr" id="edtmp_lhr" class="form-control edtmp_lhr" required="" style="text-transform: uppercase;">
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Tanggal Lahir</label>
							<input type="date" name="edtgl_lhr" id="edtgl_lhr" class="form-control edtgl_lhr" required="" style="text-transform: uppercase;">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Gender</label>
							<select class="form-control select edgender" id="edgender" required="">
								<option value="LAKI-LAKI">LAKI-LAKI</option>
								<option value="PEREMPUAN">PEREMPUAN</option>
							</select>
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Agama</label>
							<select class="form-control select edagama" id="edagama" required="">
								
								<option value="KRISTEN">KRISTEN</option>
								<option value="ISLAM">ISLAM</option>
								<option value="KATOLIK">KATOLIK</option>
								<option value="HINDU">HINDU</option>
								<option value="BUDHA">BUDHA</option>
								<option value="KONGHUCU">KONGHUCU</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">No. Telp</label>
							<input type="text" name="ednohp" id="ednohp" class="form-control ednohp" required="" style="text-transform: uppercase;">
						</div>
						<div class="col-lg-3">
							<label class="display-block text-semibold">Kecamatan</label>
							<select id="edidkec" class="form-control select edidkec" required="" onchange="ajaxsetKelEd();">

							</select>
						</div>
						<div class="col-lg-3">
							<label class="display-block text-semibold">Kelurahan</label>
							<input type="text" name="txedkel" id="txedkel" class="txedkel hidden">
							<select id="edidkel" class="form-control select edidkel" required=""></select>
						</div>
						<div class="col-lg-12">
							<label class="display-block text-semibold">Alamat</label>
								<input type="text" name="edalamat" id="edalamat" class="form-control edalamat" required="" style="text-transform: uppercase;">
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" id="btn-save" class="btn btn-primary"><span class="icon-address-book"></span> Simpan</button>
							<button type="button" id="btn-save" class="btn btn-warning" onclick="dispose();"><span class="icon-x"></span> Batal</button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
