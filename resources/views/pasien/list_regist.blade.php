@extends('layouts.app', ['active' => 'regist_periksa'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Pasien</a></li>
			<li class="active">Pendaftaran Periksa</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row form-group">
					<form action="{{ route('pasien.ajaxGetPasienPeriksa') }}" id="form-search" >
						<label><b>Cari Pasien</b></label>
						<div class="input-group">
							<input type="text" name="key" id="key" required="" class="form-control" placeholder="#search in here">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-primary" id="btn-search">Cari</button>
							</div>
						</div>
					</form>
				</div>
				<br>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>NO. RM</th>
									<th>NIK</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_riwayat" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Riwayat Periksa</h5>
			</div>

			<div class="modal-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-riwayat">
						<thead>
							<tr>
								<th>#</th>
								<th>Tanggal Periksa</th>
								<th>Poli</th>
								<th>Dokter</th>
								<th>Status</th>
								<th>Tindakan</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Daftar Periksa</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('pasien.addPeriksa') }}" id="form-daftar">
					<div class="row">
						<div class="col-lg-4">
							<label>No. RM</label>
							<input type="text" name="txrm" id="txrm" class="form-control" readonly="">
						</div>
						<div class="col-lg-4">
							<label>NIK</label>
							<input type="text" name="txnik" id="txnik" class="form-control" readonly="">
						</div>
						<div class="col-lg-4">
							<label>Nama</label>
							<input type="text" name="txnama" id="txnama" class="form-control" readonly="">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label>Poli</label>
							<select id="lpoli" class="form-control select" onchange="ajaxDokter(this);">
								
							</select>
						</div>
						<div class="col-lg-6">
							<label>Dokter</label>
							<select id="ldokter" class="form-control select">
								<option value="">--Pilih Dokter--</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4">
							<button type="submit" class="btn btn-primary" id="btn-save"><span class="icon-quill2"></span> Simpan</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: $('#form-search').attr('action'),
           data: function (d) {
	            return $.extend({},d,{
	                "key": $('#key').val()
	            });
	        }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'user_id', name: 'user_id'},
            {data: 'nik', name: 'nik'},
            {data: 'nama', name: 'nama'},
            {data: 'alamat', name: 'alamat'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
	});

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	});

	$('#table-list').on('click','.riwayat',function(){
		var rm_id = $(this).data('id');
		ajaxRiwayat(rm_id);
	});

	$('#table-list').on('click','.regist',function(event){
		event.preventDefault();
		var id = $(this).data('id');
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  "{{ route('pasien.ajaxGetPasien') }}",
	        data:{id:id},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var data = response.data;

	         	$('#txrm').val(data.user_id);
	         	$('#txnik').val(data.nik);
	         	$('#txnama').val(data.nama);
	         	ajaxPoli();

	         	$('#modal_add').modal('show');
	         	$.unblockUI();
	        },
	        error: function(response) {
	           // var notif = response.data;
            //     alert(notif.status,notif.output);
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	$("#form-daftar").submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-daftar').attr('action'),
	        data : {user_id:$('#txrm').val() ,poli:$('#lpoli').val() ,dokter:$('#ldokter').val()},
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
                 $.unblockUI();
                 $('#modal_add').modal('hide');
	        },
	        error: function(response) {
	           var notif = response.data;
                alert(notif.status,notif.output);
                 $.unblockUI();
	            
	        }
	    });
	})
});

function ajaxRiwayat(rm_id){
	

	var tableR = $('#table-riwayat').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{route('pasien.ajaxGetRiwayat') }}",
           data: function (d) {
	            return $.extend({},d,{
	                "id": rm_id
	            });
	        }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'nama_poli', name: 'nama_poli'},
            {data: 'nama_dokter', name: 'nama_dokter'},
            {data: 'status', name: 'status'},
            {data: 'tindakan', name: 'tindakan'},
            // {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
	});


	$('#modal_riwayat').modal('show');
}

function ajaxPoli(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url :  "{{ route('pasien.poli') }}",
        success: function(response) {

         	var poli = response.poli;

         	$('#lpoli').empty();
         	$('#lpoli').append('<option value="">--Pilih Poli--</option>');
         	for (var i = 0; i < poli.length; i++) {
         		$('#lpoli').append('<option value="'+poli[i]['id']+'">'+poli[i]['nama_poli']+'</option>');
         	}
        },
        error: function(response) {
           console.log(response);
            
        }
    });
}

function ajaxDokter(e){
	var poli = $('#lpoli').val();

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url :  "{{ route('pasien.dokter') }}",
        data : {id_poli:poli},
        success: function(response) {

         	var dokt = response.dokter;

         	$('#ldokter').empty();
         	$('#ldokter').append('<option value="">--Pilih Dokter--</option>');
         	for (var i = 0; i < dokt.length; i++) {
         		$('#ldokter').append('<option value="'+dokt[i]['id']+'">'+dokt[i]['nama_dokter']+'</option>');
         	}
        },
        error: function(response) {
           console.log(response);
            
        }
    });
}
</script>
@endsection