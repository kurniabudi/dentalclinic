@extends('layouts.app', ['active' => 'kasir'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Layanan</a></li>
			<li class="active">Kasir</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">

				<div class="row form-group">
					<!-- <form action="{{ route('layanan.kasir.ajaxGetPeriksa') }}" id="form-search">
						<div class="col-lg-11">
							<label>Cari Nomor Periksa</label>
							<input type="text" name="noperiksa" class="form-control" id="noperiksa" required="">
						</div>
						<div class="col-lg-1">
							<button type="submit" id="cari" class="btn btn-primary" style="margin-top: 25px;"><span class=" icon-search4" ></span> Cari</button>
						</div>
					</form> -->

					<div class="col-md-12">
						<select class="form-control select-search" id="pilpasien">
							<option value="">--Pilih Pasien--</option>

							@foreach($pasien as $ps)

								<option value="{{$ps->id}}">{{$ps->id}} - {{$ps->nama}} ({{$ps->user_id}}) - {{$ps->status}}</option>
							@endforeach
						</select>
					</div>
				</div>				
			</div>
		</div>
		<div class=" panel panel-flat hidden" id="panel-detail">
			<div class="panel-body">
				<div class="row form-group">
					<div class="col-lg-4">
						<label>NIK</label>
						<input type="text" name="txnik" id="txnik" class="form-control" readonly="">
					</div>
					<div class="col-lg-4">
						<label>Nama Pasien</label>
						<input type="text" name="txnama" id="txnama" class="form-control" readonly="">
					</div>
					<div class="col-lg-4">
						<label>Tempat Tanggal Lahir</label>
						<input type="text" name="txttl" id="txttl" class="form-control" readonly="">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-3">
						<label>No. Periksa</label>
						<input type="text" name="txnopr" id="txnopr" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>No. RM</label>
						<input type="text" name="txrm" id="txrm" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Poli</label>
						<input type="text" name="txpoli" id="txpoli" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Dokter</label>
						<input type="text" name="txdok" id="txdok" class="form-control" readonly="">
					</div>
				</div>
				<br>
				<div class="row form-group" id="sel-periksa">
					<div class="col-lg-12">
						<label><b>Jenis Pelayanan</b></label>
						<select id="add-periksa" class="form-control select-search">
							
						</select>
					</div>
				</div>
				<br>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-resep">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Product / PLU</th>
									<th>Nama Product</th>
									<th>Qty</th>
									<th>Harga (satuan)</th>
									<th>Sub Total</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr>
									<td colspan="4"></td>
									<td><b>Total Resep</b></td>
									<td><label id="lb_totresep" style="font-weight: bold;"></label></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<br>
				<div class="row form-group">
					<div class="col-lg-4">
						<label><b>Total Sebelum</b></label>
						<input type="text" name="oldtot" id="oldtot" class="form-control" readonly="">
					</div>
					<div class="col-lg-4">
						<label><b>Total Diskon Sebelum</b></label>
						<input type="text" name="olddisk" id="olddisk" class="form-control" readonly="">
					</div>
					<div class="col-lg-4">
						<label><b>Total Pembayaran Sebelum</b></label>
						<input type="text" name="oldbayar" id="oldbayar" class="form-control" readonly="">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-2">
						<label>Kewajiban <b>(Rp.)</b></label>
						<input type="text" name="wbyr" id="wbyr" class="form-control" readonly="">
					</div>
					<div class="col-lg-2">
						<label>Diskon <b>(%)</b></label>
						<input type="number" min="0" name="disk" id="disk" class="form-control"  onkeyup="autoCalc();">
						<input type="number" min="0" name="rdisk" id="rdisk" class="form-control hidden">
					</div>
					<div class="col-lg-2">
						<label>Jumlah Bayar <b>(Rp.)</b></label>
						<input type="number" min="0" name="jbyr" id="jbyr" class="form-control" readonly="" >
					</div>
					<div class="col-lg-2">
						<label>Bayar <b>(Rp.)</b></label>
						<input type="number" min="0" name="bayar" id="bayar" class="form-control" onkeyup="autoCalc();">
					</div>
					<div class="col-lg-2">
						<label>Kembali <b>(Rp.)</b></label>
						<input type="text" name="kembali" id="kembali" class="form-control" readonly="" value="0">
					</div>
					<div class="col-lg-2">
						<center>
							<button class="btn btn-success" id="btn-save" style="margin-top: 25px;"><span class="icon-cart-add2"></span> Simpan</button>
							<button class="btn btn-warning hidden" id="btn-print" style="margin-top: 25px;"><span class="icon-printer4"></span> Print</button>
						</center>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>

@endsection




@section('js')
<script type="text/javascript">
$(document).ready(function(){
	
	// $('#form-search').submit(function(event){
	// 	event.preventDefault();
	// 	$('#btn-save').removeClass('hidden');
	// 	$.ajaxSetup({
	//         headers: {
	//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	//         }
	//     });
	//     $.ajax({
	//         type: 'get',
	//         url :  $('#form-search').attr('action'),
	//         data : {noperiksa:$('#noperiksa').val()},
	//         beforesend:function(){
	//         	loading();
	//         },
	//         success: function(response) {
	//         	var perik = response.periksa;
	        	
	//         	if (perik.gender=='LAKI-LAKI') {
	//         		var gend = '(L) ';
	//         	}else{
	//         		var gend = '(P) '
	//         	}
	//         	$('#txnik').val(perik.nik);
	//         	$('#txnama').val(gend+perik.nama);
	//         	$('#txttl').val(perik.tempat_lahir+' '+perik.tanggal_lahir);
	//         	$('#txrm').val(perik.user_id);
	//         	$('#txnopr').val(perik.id);
	//         	$('#txpoli').val(perik.nama_poli);
	//         	$('#txdok').val(perik.nama_dokter);

	//         	if (perik.status=='CLOSE') {
	//         		$('#sel-periksa').addClass('hidden');
	        		
	//         		document.getElementById("disk").readOnly = true;
	//         		document.getElementById("bayar").readOnly = true;
	//         		$('#btn-save').addClass('hidden');
	//         	}
	//         	var bayar = response.bayar;

	//         	if (bayar.status!=null) {
	//         		$('#btn-print').removeClass('hidden');
	//         	}

	//         	var disk = $("#disk").val();
	//         	$('#oldtot').val(bayar.tot_old);
	//         	$('#olddisk').val(bayar.tot_disk);
	//         	$('#oldbayar').val(bayar.tot_bayar);
	//         	$('#wbyr').val(bayar.kurang_byr);
	        	
	//         	autoCalc();
	//         	detail(response.resep);
	//         	listBea();
	//         	$('#panel-detail').removeClass('hidden');
 //                 $.unblockUI();
	//         },
	//         error: function(response) {
	          
	        
 //                alert(422,"Nomor Periksa Tidak ditemukan");
	//         }
	//     });
	// });
	$('#pilpasien').change(function(event){
		event.preventDefault();
		var id = $('#pilpasien').val();
		// $('#btn-save').removeClass('hidden');
		if (id!="") {
			$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url : "{{ route('layanan.kasir.ajaxGetPeriksa') }}",
	        data : {noperiksa:id},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var perik = response.periksa;
	        	
	        	if (perik.gender=='LAKI-LAKI') {
	        		var gend = '(L) ';
	        	}else{
	        		var gend = '(P) '
	        	}
	        	$('#txnik').val(perik.nik);
	        	$('#txnama').val(gend+perik.nama);
	        	$('#txttl').val(perik.tempat_lahir+' '+perik.tanggal_lahir);
	        	$('#txrm').val(perik.user_id);
	        	$('#txnopr').val(perik.id);
	        	$('#txpoli').val(perik.nama_poli);
	        	$('#txdok').val(perik.nama_dokter);
	        	console.log(perik.status);
	        	if (perik.status=='CLOSE') {
	        		$('#sel-periksa').addClass('hidden');
	        		
	        		document.getElementById("disk").readOnly = true;
	        		document.getElementById("bayar").readOnly = true;
	        		// document.getElementById("btn-save").readOnly = true;
	        		$('#btn-save').addClass('hidden');
	        	}
	        	var bayar = response.bayar;

	        	if (bayar.status!=null) {
	        		$('#btn-print').removeClass('hidden');
	        	}

	        	var disk = $("#disk").val();
	        	$('#oldtot').val(bayar.tot_old);
	        	$('#olddisk').val(bayar.tot_disk);
	        	$('#oldbayar').val(bayar.tot_bayar);
	        	$('#wbyr').val(bayar.kurang_byr);
	        	
	        	autoCalc();
	        	detail(perik.id);
	        	listBea();
	        	$('#panel-detail').removeClass('hidden');
                 $.unblockUI();
	        },
	        error: function(response) {
	          
	        
                alert(422,"Nomor Periksa Tidak ditemukan");
	        }
	    });
		}else{
			alert(422,"Nomor Periksa kosong ! ! !");
			return false;
		}
			
	});

	// $('#disk').on('keyup',function(){
	// 	var txpemb = $('#wbyr').val() - $('#disk').val();

	// 	$('#jbyr').val(txpemb);
	// });

	// $('#bayar').on('keyup',function(){
	// 	var kemb  = $('#bayar').val() - $('#jbyr').val();

	// 	$('#kembali').val(kemb);
	// });

	$('#btn-save').click(function(event){
		event.preventDefault();
			var bayar = $('#bayar').val();
			var pemby = $('#jbyr').val();
			var kembl = $('#kembali').val();
			var wbyr = $('#wbyr').val();
			var xbyr = 0;

			if (kembl >=0) {
				xbyr = pemby;
			}else{
				xbyr = bayar;
			}
		
		if (wbyr ==0) {
			alert(422,"Tidak ada Tagihan ! ! !");
			return false;
		}
			
		if (pemby =="") {
			alert(422,"Pembayaran belum diisi ! ! !");
			return false;
		}

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('layanan.kasir.addPembayaran') }}",
	        data : {idperiksa:$('#txnopr').val(),total:$('#wbyr').val(),bayar:xbyr,kembali:$('#kembali').val(),diskon:$('#rdisk').val()},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var notif = response.data;
                alert(notif.status,notif.output);
                 
                 print($('#txnopr').val());
                 $('#form-search').submit();
                 $.unblockUI();
                 $('#disk').val('');
                 $('#jbyr').val('');
                 $('#bayar').val('');
                 $('#29999').val('');
                 // window.location.reload();
	        },
	        error: function(response) {
	          
	        	$.unblockUI();
                alert(response.status,response.responseText);
	        }
	    });
	});


	$('#add-periksa').on('change',function(){
		var id  = $('#add-periksa').val();

		if (id=="" || id==null) {
			alert(422,"Jenis Layanan Belum dipilih");
			return false;
		}

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('layanan.kasir.addBeaPeriksa') }}",
	        data : {id:id,noperiksa:$('#txnopr').val()},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var notif = response.data;
	            alert(notif.status,notif.output);
	            detail($('#pilpasien').val());
	            $.unblockUI();
	            
	        },
	        error: function(response) {
	          
	        	$.unblockUI();
	            alert(response.status,response.responseText);
	        }
	    });
	});

	$('#btn-print').click(function(event){
		event.preventDefault();

		print($('#txnopr').val());
	});
	
});

function detail(id_perik){

	var totresp = 0;
	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  "{{ route('layanan.kasir.getResep') }}",
	        data : {id:id_perik},
	        success: function(response) {
	        	// console.log(response.resep);
	        	var rsp = response.resep;
	            $('#table-resep > tbody').empty();
				for (var i = 0; i < rsp.length; i++) {
					totresp = totresp + rsp[i]['sub_total'];
					var no = i+1;
					$('#table-resep > tbody').append(
						"<tr><td>"+no+"</td><td>"+rsp[i]['id']+" ("+rsp[i]['kode_product']+")</td><td>"+rsp[i]['nama_product']+"</td><td>"+rsp[i]['qty']+" "+rsp[i]['nama_satuan']+"</td><td>"+rsp[i]['harga']+"</td><td>"+rsp[i]['sub_total']+"</td><td><button class='btn btn-warning' data-id='"+rsp[i]['listid']+"' onclick='delItem(this);'>DEL</button></td></tr>"
						);
				}

				$('#lb_totresep').text(totresp);
	        },
	        error: function(response) {
	          
	        	$.unblockUI();
	            alert(response.status,response.responseText);
	        }
	    });

}

function listBea(){
	$.ajax({
	    type: 'get',
	    url :  "{{ route('layanan.kasir.listBea') }}",

	    success: function(response) {
	    	var lb = response.listbea;

	    	$('#add-periksa').empty();
	    	$('#add-periksa').append('<option value="">--Pilih Jenis Layanan--</option>');
	    	for (var i = 0; i < lb.length; i++) {
	    		$('#add-periksa').append('<option value="'+lb[i]['id']+'&'+lb[i]['harga']+'">'+lb[i]['nama_product']+'</option>');
	    	}
	    },
	    error: function(response) {
	      console.log(response);
	    }
	});
}

function print(id){
	var link = "{{ route('layanan.kasir.printNota') }}";
	var url = link+'?id='+id;
	window.open(url);
}

function delItem(e){
	var id = e.getAttribute('data-id');
	
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url :  "{{ route('layanan.kasir.deleteItem') }}",
        data : {id:id},
        beforesend:function(){
        	loading();
        },
        success: function(response) {
        	var notif = response.data;
            alert(notif.status,notif.output);
            // $('#form-search').submit();
             detail($('#pilpasien').val());
            $.unblockUI();
            
        },
        error: function(response) {
          
        	$.unblockUI();
            alert(response.status,response.responseText);
        }
    });
}

function autoCalc(){
	var wjbyr = $('#wbyr').val();
	var diskn = 0;
	
	var bbyr = 0;



	if ($('#disk').val()=="") {
		diskn = 0;
	}else{
		diskn = wjbyr * ($('#disk').val()/100);
	}


	$('#rdisk').val(diskn);
	if ($('#bayar').val()=="") {
		bbyr = 0;
	}else{
		bbyr = $('#bayar').val();
	}

	var hjbyr = wjbyr - diskn;

	$('#jbyr').val(hjbyr);

	var jmlbyr = $('#jbyr').val();
	var hkmb = bbyr - jmlbyr;
	$('#kembali').val(hkmb);

}




</script>
@endsection
