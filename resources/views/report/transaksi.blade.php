@extends('layouts.app', ['active' => 'report_transaksi'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Report</a></li>
			<li class="active">Transaksi</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<form action="{{ route('report.transaksi.ajaxGetTrans') }}" id="form-search">
						<div class="col-lg-11">
							<label><b>Tanggal Transaksi</b></label>
							<input type="text" name="tanggal" id="tanggal" class="form-control daterange-basic" required="">
						</div>
						<div class="col-lg-1">
							<button type="submit" class="btn btn-primary" id="btn-cari" style="margin-top: 30px;"><span class="icon-search4"></span> Cari</button>
						</div>
					</form>
				</div>
				<hr>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>Tanggal</th>
									<th>No. Transaksi</th>
									<th>No. Pemeriksaan</th>
									<th>No. RM</th>
									<th>Nama</th>
									<th>Total Transaksi</th>
									<th>Total Bayar</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('report.transaksi.exportTrans') }}" id="export"></a>
@endsection



@section('js')
<script type="text/javascript" src="{{url('assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var _token = $("input[name='_token']").val();
    var table = $('#table-list').DataTable({
        buttons: [
            {
                text: 'Export to Excel',
                className: 'btn btn-sm bg-success exportExcel',
                action: function (e, dt, node, config)
                {
                    var columns = table.settings().init().columns;
                    var current_order = table.order();

                    var column_name = columns[current_order[0][0]].name;
                    var direction = current_order[0][1];
                    var filter = table.search();
                    window.location.href = $('#export').attr('href')
                                            + '?tanggal=' + $('#tanggal').val()
                                            + '&filterby=' + filter;
                }
            }
       ],
        ajax: {
	        type: 'GET',
	        url: $('#form-search').attr('action'),
	        data: function (d) {
	            return $.extend({},d,{
	            	"tanggal":$('#tanggal').val()
	            });
	        }
	    },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            }
        ],
        columns: [
	        {data: null, sortable: false, orderable: false, searchable: false},
	        {data: 'created_at', name: 'created_at'},
	        {data: 'id', name: 'id'},
	        {data: 'id_pemeriksaan', name: 'id_pemeriksaan'},
	        {data: 'user_id', name: 'user_id'},
	       	{data: 'nama', name: 'nama'},
	        {data: 'total', name: 'total'},
	        {data: 'tot_bayar', name: 'tot_bayar'}
	    ]
    });

    table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#form-search').submit(function(event){
		event.preventDefault();
		
		table.clear();
		table.draw();
	});
});






</script>
@endsection