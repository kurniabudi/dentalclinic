<style type="text/css">
@page{
    margin : 5 5 5 5;

}

/*@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}*/

.br{
    line-height: 3px;
    margin-top: 5px;
}

</style>

<div style="border:1px solid black; width: 100%; height: auto;">
    <center>
        <table id="kop" width="100%">
            <tr>
                <td rowspan="3" width="80">
                    <center>
                         <img src="{{ public_path('assets/icon/gigi2.png') }}" alt="Image"width="80" >
                    </center>
                  
                </td>
                <td>
                    <h3 style="margin-left: 110px; margin-top: -1; margin-bottom: -2px;">KURNIA WAHAB DENTAL CARE</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <h5 style="margin-left: 15px; margin-top: -2; margin-bottom: -2px;">Jl. Tjilik Riwut RT.13 RW.04 Kel. Mendawai, Sukamara (Depan SMPN 1 Sukamara)</h5>
                </td>
            </tr>
            <tr>
                <td>
                    <h5 style="margin-left: 220px; margin-top: -2; margin-bottom: -2px;">Telp. 0853887404</h5>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
        <table id="tb-head" width="100%">
            <tr>
                <td width="20%"><label style="font-size: 12px;">ID. REG</label></td>
                <td>:</td>
                <td width="32%"><label style="font-size: 12px;">{{ $head->id }}</label></td>

                <td width="20%"><label style="font-size: 12px;">TANGGAL</label></td>
                <td>:</td>
                <td width="32%"><label style="font-size: 12px;">{{ date_format(date_create($head->created_at),' d F Y')}}</label></td>
            </tr>
            <tr>
                <td><label style="font-size: 12px;">No. RM / NAMA</label></td>
                <td>:</td>
                <td><label style="font-size: 12px;">{{ $head->user_id }} / {{ $head->nama }}</label></td>

          

                <td><label style="font-size: 12px;">DOKTER / POLI</label></td>
                <td>:</td>
                <td><label style="font-size: 12px;">{{ $head->nama_dokter }} / {{ $head->nama_poli }}</label></td>
            </tr>
        </table>
        <hr>
        <table id="tb-det" width="100%" >
            <tr>
                <td colspan="5"><center><label style="font-size:13; font-weight: bold;">Daftar Biaya</label></center></td>
            </tr>
            <tr style="font-size:13px; font-weight: bold; text-align:center;">
                <td>#</td>
                <td>Item</td>
                <td>Qty</td>
                <td>Harga Satuan</td>
                <td>Sub. Total</td>
            </tr>
            <tr>
                <td colspan="5"><hr></td>
            </tr>
            <?php 
                $i = 1;
                $tot_qty = 0;
                foreach ($detl as $dt){
                $tot_qty = $tot_qty + $dt->qty;


            ?>
                <tr style="font-size:12px; text-align:center;">
                    <td>{{$i++}}</td>
                    <td>{{$dt->nama_product}}</td>
                    <td>{{$dt->qty}}</td>
                    <td>Rp. {{$dt->harga}}</td>
                    <td>Rp. {{$dt->sub_total}}</td>
                </tr>
            <?php 
                } 
            ?>
            <tr>
                <td colspan="5"><hr></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td><center>Total</center></td>
                <td><center>Rp.{{$total->tot_bea}}</center></td>
            </tr>
            <tr>
                
                <td><center>Kasir</center></td>
                <td colspan="2"></td>
                <td><center>Diskon</center></td>
                <td><center>Rp.{{$total->tot_disk}}</center></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td><center>Terbayar</center></td>
                <td><center>Rp.{{$total->tot_bayar}}</center></td>
            </tr>
            <tr>
                <td><center> {{Auth::user()->name }} </center></td>
                <td colspan="2"></td>
                <td><center>Kurang</center></td>
                <td><center>Rp.{{$total->kurang_byr}}</center></td>
            </tr>
        </table>
       <!--  <hr>
        <table id="tb-tagihan" width="100%"  >
           <tr>
               <td><b><center>Tagihan</center></b></td>
               <td><b><center>Diskon</center></b></td>
               <td><b><center>Kewajiban</center></b></td>
               <td><b><center>Bayar</center></b></td>
               <td><b><center>Kurang</center></b></td>
           </tr>
           <tr>
               <td><b><center>Rp.{{$pemby->total}}</center></b></td>
               <td><b><center>Rp.{{$pemby->potongan}}</center></b></td>
               <td><b><center>Rp.{{$pemby->total - $pemby->potongan}}</center></b></td>
               <td><b><center>Rp.{{$pemby->bayar}}</center></b></td>
               <td><b><center>Rp.{{$pemby->kurang}}</center></b></td>
           </tr>
        </table>
        <hr>
        <table id="tb-tagihan" width="100%"  >
           <tr>
               <td><b><center>Total Biaya</center></b></td>
               <td><b><center>Total Diskon</center></b></td>
               <td><b><center>Total Bayar</center></b></td>
               <td><b><center>Kurang</center></b></td>
           </tr>
           <tr>
               <td><b><center>Rp.{{$total->tot_bea}}</center></b></td>
               <td><b><center>Rp.{{$total->tot_disk}}</center></b></td>
               <td><b><center>Rp.{{$total->tot_bayar}}</center></b></td>
               <td><b><center>Rp.{{$total->kurang_byr}}</center></b></td>
           </tr>
        </table>
        <hr> -->
        
    </center>
</div>
