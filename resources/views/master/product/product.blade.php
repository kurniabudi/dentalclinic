@extends('layouts.app', ['active' => 'list_product'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Product</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-3">
						<button id="btn-add" class="btn btn-default"><span class="icon-folder-plus"></span> Tambah</button>
					</div>
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Product</th>
									<th>PLU</th>
									<th>Nama Product</th>
									<th>Harga</th>
									<th>Satuan</th>
									<th>Kategori</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Product</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.product.addProduct') }}" id="form-add">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama Product</label>
							<input type="text" name="txnama" id="txnama" class="form-control" required="" style="text-transform: uppercase;" placeholder="Nama Product">
						</div>	
						<div class="col-lg-6">
							<label class="display-block text-semibold">PLU / Kode Product</label>
							<input type="text" name="txkodeprod" id="txkodeprod" class="form-control" required="" style="text-transform: uppercase;" placeholder="PLU / Kode Product">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Satuan</label>
							<select id="txsatuan" class="form-control select-search" required=""></select>
						</div>	
						<div class="col-lg-6">
							<label class="display-block text-semibold">Kategory</label>
							<select id="txkategory" class="form-control select-search" required=""></select>
						</div>
					</div>	
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Harga</label>
							<input type="number" name="txharga" id="txharga" class="form-control" required="">
						</div>	
						<div class="col-lg-6">
							<button type="submit" class="btn btn-success" style="margin-top: 30px;"><span class="icon-file-download2"></span> Simpan</button>
						</div>
					</div>				
				</form>
			</div>
		</div>
	</div>
</div>

<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Product</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.product.editProduct') }}" id="form-edit">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama Product</label>
							<input type="text" name="ednama" id="ednama" class="form-control" required="" style="text-transform: uppercase;" placeholder="Nama Product">
							<input type="text" name="edid" id="edid" class="hidden">
						</div>	
						<div class="col-lg-6">
							<label class="display-block text-semibold">PLU / Kode Product</label>
							<input type="text" name="edkodeprod" id="edkodeprod" class="form-control" required="" style="text-transform: uppercase;" placeholder="PLU / Kode Product">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Satuan</label>
							<select id="edsatuan" class="form-control select-search" required=""></select>
						</div>	
						<div class="col-lg-6">
							<label class="display-block text-semibold">Kategory</label>
							<select id="edkategory" class="form-control select-search" required=""></select>
						</div>
					</div>	
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Harga</label>
							<input type="number" name="edharga" id="edharga" class="form-control" required="">
						</div>	
						<div class="col-lg-6">
							<button type="submit" class="btn btn-success" style="margin-top: 30px;"><span class="icon-file-download2"></span> Ubah</button>
						</div>
					</div>				
				</form>
			</div>
		</div>
	</div>
</div>


@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	

	

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master.product.ajaxGetProduct') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'kode_product', name: 'kode_product'},
            {data: 'nama_product', name: 'nama_product'},
            {data: 'harga', name: 'harga'},
            {data: 'nama_satuan', name: 'nama_satuan'},
            {data: 'nama_kategory', name: 'nama_kategory'},
            {data: 'action', name: 'action', sortable: false, orderable: false, searchable: false}
        ]
	});

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#btn-add').click(function(){
		$('#txkodeprod').val('');
		$('#txnama').val('');
		$('#txharga').val('');
		$('#modal_add').modal('show');
		katgSatuanAdd();
	});

	$('#form-add').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-add').attr('action'),
	        data:{nama:$('#txnama').val(),plu:$('#txkodeprod').val(),harga:$('#txharga').val(),satuan:$('#txsatuan').val(),kateg:$('#txkategory').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            table.clear();
	            table.draw();
	            $.unblockUI();
	            $('#txnama').val('');
	            $('#txkodeprod').val('');
	            $('#txharga').val('');
	            $('#txsatuan').empty();
	            $('#txkategory').empty();
				$('#modal_add').modal('hide');
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});


	$('#table-list').on('click','.edProduct',function(event){
		event.preventDefault();
		var id = $(this).data('id');

		
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  "{{ route('master.product.getProduct') }}",
	        data:{id:id},
	        beforeSend:function(){
	        	loading();
	        },
	        success: function(response) {

	         	var data = response.data;
               	
               	$('#edid').val(data.id);
               	$('#ednama').val(data.nama_product);
               	$('#edharga').val(data.harga);
               	$('#edkodeprod').val(data.kode_product);
              	katgSatuanEd(data.id_satuan,data.id_kategory);
              	$('#modal_edit').modal('show');

              	$.unblockUI();
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });

	});

	$('#form-edit').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-edit').attr('action'),
	        data:{id:$('#edid').val(),nama:$('#ednama').val(),plu:$('#edkodeprod').val(),harga:$('#edharga').val(),satuan:$('#edsatuan').val(),kateg:$('#edkategory').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            table.clear();
	            table.draw();
	            $.unblockUI();
	            $('#edid').val('');
	            $('#ednama').val('');
	            $('#edharga').val('');
	            $('#edkodeprod').val('');
	            $('#edkategory').empty();
	            $('#edsatuan').empty();
				$('#modal_edit').modal('hide');
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	$('#table-list').on('click','.delProduct',function(){
		var id = $(this).data('id');

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('master.product.delProd')}}",
	        data:{id:id},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            window.location.reload();
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});
	

	

	
});

function katgSatuanAdd(){

    $.ajax({
        type: 'get',
        url : "{{ route('master.product.ajaxListKatgSat') }}",
        success: function(response) {
        	var sat = response.satuan;
        	$('#txsatuan').empty();
        	for (var i = 0; i < sat.length; i++) {
        		$('#txsatuan').append('<option value="'+sat[i]['id']+'">'+sat[i]['nama_satuan']+'</option>');
        	}

        	var kat = response.kategory;
        	$('#txkategory').empty();
        	for (var j = 0; j < kat.length; j++) {
        		$('#txkategory').append('<option value="'+kat[j]['id']+'">'+kat[j]['nama_kategory']+'</option>');
        	}
        },
        error: function(response) {
           
        	console.log(response);
            
        }
    });
}

function katgSatuanEd(id_satuan,id_kateg){

    $.ajax({
        type: 'get',
        url : "{{ route('master.product.ajaxListKatgSat') }}",
        success: function(response) {
        	var sat = response.satuan;
        	$('#edsatuan').empty();
        	for (var i = 0; i < sat.length; i++) {
        		if (sat[i]['id']==id_satuan) {
        			var satsel = 'selected';
        		}else{
        			var satsel = '';
        		}

        		$('#edsatuan').append('<option value="'+sat[i]['id']+'" '+satsel+'>'+sat[i]['nama_satuan']+'</option>');
        	}

        	var kat = response.kategory;
        	$('#edkategory').empty();
        	for (var j = 0; j < kat.length; j++) {
        		if (kat[j]['id']==id_kateg) {
        			var katsel = 'selected';
        		}else{
        			var katsel = '';
        		}
        		$('#edkategory').append('<option value="'+kat[j]['id']+'" '+katsel+'>'+kat[j]['nama_kategory']+'</option>');
        	}
        },
        error: function(response) {
           
        	console.log(response);
            
        }
    });
}



</script>
@endsection