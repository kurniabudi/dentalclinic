@extends('layouts.app', ['active' => 'kategory_saatuan'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Kategory & Satuan</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="col-lg-6">
					<div class="row">
						<h3>Satuan</h3>
					</div>
					<div class="row">
						<button class="btn btn-primary" id="btn-satuan"><span class="icon-file-plus2"></span> Tambah</button>
					</div>
					<div class="row">
						<div class="table-responsive">
							<table class ="table table-basic table-condensed" id="table-satuan">
								<thead>
									<tr>
										<th>#</th>
										<th>ID Satuan</th>
										<th>Nama Satuan</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<h3>Kategory</h3>
					</div>
					<div class="row">
						<button class="btn btn-primary" id="btn-kateg"><span class="icon-file-plus2"></span> Tambah</button>
					</div>
					<div class="row">
						<div class="table-responsive">
							<table class ="table table-basic table-condensed" id="table-kategory">
								<thead>
									<tr>
										<th>#</th>
										<th>ID Kategory</th>
										<th>Nama Kategory</th>
										<th>Type</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_satuan" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Satuan</h5>
			</div>

			<div class="modal-body">
				<form action="{{route('master.product.addSatuan')}}" id="form-satuan">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama Satuan</label>
							<input type="text" name="txsatuan" id="txsatuan" class="form-control" required="" style="text-transform: uppercase;" placeholder="Nama Satuan">
						</div>
						<div class="col-lg-6">
							<button class="btn btn-primary" id="btn-save" style="margin-top: 30px;"><span class="icon-file-download2"></span> Simpan</button>
						</div>
					</div>					
				</form>
			</div>
		</div>
	</div>
</div>

<div id="modal_satuan_ed" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Satuan</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.product.editSatuan') }}" id="form-satuan-ed">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama Satuan</label>
							<input type="text" name="edsatuan" id="edsatuan" class="form-control" required="" style="text-transform: uppercase;" placeholder="Nama Satuan">
							<input type="text" name="idsat" id="idsat" class="hidden">
						</div>
						<div class="col-lg-6">
							<button class="btn btn-primary" id="btn-save" style="margin-top: 30px;"><span class="icon-file-download2"></span> Simpan</button>
						</div>
					</div>					
				</form>
			</div>
		</div>
	</div>
</div>

<div id="modal_kategory" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Kategory</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.product.addKategory') }}" id="form-kategory">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama Kategory</label>
							<input type="text" name="txkatg" id="txkatg" class="form-control" required="" style="text-transform: uppercase;" placeholder="Nama Kategory">
						</div>
						<div class="col-lg-3">
							<label class="display-block text-semibold">Type</label>
							<div class="row form-group">
								<label class="radio-inline"><input type="radio" name="radio_type" checked="" value="BARANG"><b>Barang</b></label>
								<label class="radio-inline"><input type="radio" name="radio_type"  value="JASA"><b>JASA</b></label>
							</div>
						</div>
						<div class="col-lg-3">
							<button class="btn btn-primary" id="btn-save" style="margin-top: 30px;"><span class="icon-file-download2"></span> Simpan</button>
						</div>
					</div>					
				</form>
			</div>
		</div>
	</div>
</div>

<div id="modal_kategory_ed" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Kategory</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.product.editKategory') }}" id="form-kategory-ed">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama Kategory</label>
							<input type="text" name="edkatg" id="edkatg" class="form-control" required="" style="text-transform: uppercase;" placeholder="Nama Kategory">
							<input type="text" name="idktg" id="idktg" class="hidden">
						</div>
						<div class="col-lg-3">
							<label class="display-block text-semibold">Type</label>
							<div class="row form-group">
								<label class="radio-inline"><input type="radio" name="edradio_type" checked="" value="BARANG"><b>Barang</b></label>
								<label class="radio-inline"><input type="radio" name="edradio_type"  value="JASA"><b>JASA</b></label>
							</div>
						</div>
						<div class="col-lg-3">
							<button class="btn btn-primary" id="btn-save" style="margin-top: 30px;"><span class="icon-file-download2"></span> Simpan</button>
						</div>
					</div>					
				</form>
			</div>
		</div>
	</div>
</div>


@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	

	

	var tableS = $('#table-satuan').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master.product.ajaxGetSatuan') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableS.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'nama_satuan', name: 'nama_satuan'},
            {data: 'action', name: 'action'}
        ]
	});

	tableS.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	

	$('#btn-satuan').click(function(){
		$('#txsatuan').val('');
		$('#modal_satuan').modal('show');
	});

	$('#form-satuan').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-satuan').attr('action'),
	        data:{nama_satuan:$('#txsatuan').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            tableS.clear();
	            tableS.draw();
	            $.unblockUI();
	            $('#txsatuan').val('');
				$('#modal_satuan').modal('hide');
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	$('#table-satuan').on('click','.edsat',function(event){
		$('#edsatuan').val($(this).data('nama'));
		$('#idsat').val($(this).data('id'));

		$('#modal_satuan_ed').modal('show');
	});


	$('#form-satuan-ed').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  $('#form-satuan-ed').attr('action'),
	        data:{nama_satuan:$('#edsatuan').val(),id:$('#idsat').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            tableS.clear();
	            tableS.draw();
	            $.unblockUI();
	            $('#edsatuan').val('');
				$('#modal_satuan_ed').modal('hide');
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	$('#table-satuan').on('click','.delsat',function(event){

		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  "{{ route('master.product.deleteSatuan') }}",
	        data:{id:$(this).data('id')},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            tableS.clear();
	            tableS.draw();
	            $.unblockUI();

	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});


	var tableK = $('#table-kategory').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master.product.ajaxGetKategory') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableK.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'nama_kategory', name: 'nama_kategory'},
            {data: 'type', name: 'type'},
            {data: 'action', name: 'action'}
        ]
	});

	tableK.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	

	$('#btn-kateg').click(function(){
		$('#txkatg').val('');
		$('#modal_kategory').modal('show');
	});

	$('#form-kategory').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-kategory').attr('action'),
	        data:{nama_kategory:$('#txkatg').val(),type:$('input[name=radio_type]:checked').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            tableK.clear();
	            tableK.draw();
	            $.unblockUI();
	            $('#txkatg').val('');
				$('#modal_kategory').modal('hide');
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	$('#table-kategory').on('click','.edktg',function(event){
		$('#edkatg').val($(this).data('nama'));
		$('#idktg').val($(this).data('id'));

		// $("input:radio[name=edradio_type]:checked").val($(this).data('type'));
		$("input[name=edradio_type][value=" + $(this).data('type') + "]").prop('checked',true);
		$('#modal_kategory_ed').modal('show');
	});


	$('#form-kategory-ed').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  $('#form-kategory-ed').attr('action'),
	        data:{nama_kategory:$('#edkatg').val(),type:$('input[name=edradio_type]:checked').val(),id:$('#idktg').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            tableK.clear();
	            tableK.draw();
	            $.unblockUI();
	      
				$('#modal_kategory_ed').modal('hide');
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	$('#table-kategory').on('click','.delktg',function(event){
		event.preventDefault();


		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url : "{{ route('master.product.deleteKategory') }}",
	        data:{id:$(this).data('id')},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            tableK.clear();
	            tableK.draw();
	            $.unblockUI();
	      
			
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});


	

	$(window).on('load',function(){
		
		tableS.clear();
		tableS.draw();

		tableK.clear();
		tableK.draw();
		
	});

	
});





</script>
@endsection