@extends('layouts.app', ['active' => 'list_kec'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Kecamatan</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row form-group">
					<div class="col-lg-4">
						<label>ID Kecamatan</label>
						<input type="text"  name="id_kec" id="id_kec" value="{{$idkec}}" readonly="" class="form-control">
					</div>
					<div class="col-lg-4">
						<label>Nama Kecamatan</label>
						<input type="text" name="nm_kec" id="nm_kec" class="form-control" placeholder="Nama Kecamatan" required="">
					</div>
					<div class="col-lg-2">
						<button class="btn btn-primary" id="btn-save" style="margin-top: 30px;">Simpan</button>
					</div>
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Kec.</th>
									<th>Nama Kecamatan</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('modal')
<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Pasien Baru</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.alamat.editKec') }}" id="form-edit">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label><b>ID Kec</b></label>
							<input type="text" name="txid" id="txid" class="form-control txid" readonly="">
						</div>

						<div class="col-lg-6">
							<label><b>Nama Kecamatan</b></label>
							<input type="text" name="txnama" id="txnama" class="form-control txnama" required="">
						</div>
					</div>
					
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" id="btn-save" class="btn btn-primary"><span class="icon-address-book"></span> Simpan</button>
							<button type="button" id="btn-save" class="btn btn-warning" onclick="dispose();"><span class="icon-x"></span> Batal</button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function(){
	

	

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master.alamat.ajaxGetKec') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'nama_kecamatan', name: 'nama_kecamatan'},
            {data: 'action', name: 'action'}
        ]
	});

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$('#btn-save').click(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('master.alamat.addKec') }}",
	        data:{id:$('#id_kec').val(),nm_kec:$('#nm_kec').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            window.location.reload();
	        },
	        error: function(response) {
	           // var notif = response.data;
            //     alert(notif.status,notif.output);
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});


	$('#table-list').on('click','.edKec',function(event){
		event.preventDefault();

		var id = $(this).data('id');
		var nama = $(this).data('nama');

		
		$('#modal_edit').modal('show');

		$('#modal_edit .txid').val(id);
		$('#modal_edit .txnama').val(nama);
	});


	$('#modal_edit').on('hide',function(){
		$('#txid').val('');
		$('#txnama').val('');
	});


	$('#form-edit').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-edit').attr('action'),
	        data:{id:$('#txid').val(),nm_kec:$('#txnama').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            window.location.reload();
	        },
	        error: function(response) {
	           // var notif = response.data;
            //     alert(notif.status,notif.output);
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});
	
	$('#table-list').on('click','.delKec',function(event){
		event.preventDefault();

		var id = $(this).data('id');
		
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('master.alamat.delKec') }}",
	        data:{id:id},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            window.location.reload();
	        },
	        error: function(response) {
	           // var notif = response.data;
            //     alert(notif.status,notif.output);
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});
	

	

	

	

	
});





</script>
@endsection