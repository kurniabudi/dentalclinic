@extends('layouts.app', ['active' => 'list_dokter'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Dokter</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-3">
						<button id="btn-add" class="btn btn-default"><span class="icon-user-plus"></span> Tambah</button>
					</div>
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>No. ID</th>
									<th>Nama</th>
									<th>Poli</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Dokter</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.dokter.addDokter') }}" id="form-add">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">NID</label>
							<input type="text" name="nid" id="nid" class="form-control" required="" style="text-transform: uppercase;">
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama</label>
							<input type="text" name="nama" id="nama" class="form-control" required="" style="text-transform: uppercase;">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Poli</label>
							<select id="poli" class="form-control select">
								
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" id="btn-save" class="btn btn-primary"><span class="icon-address-book"></span> Simpan</button>
							<button type="button" id="btn-save" class="btn btn-warning" onclick="dispose();"><span class="icon-x"></span> Batal</button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>


<div id="modal_edit" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Dokter</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.dokter.editDokter') }}" id="form-edit" >
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">NID</label>
							<input type="text" name="enid" id="enid" class="form-control" required="" style="text-transform: uppercase;" readonly="">
						</div>
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama</label>
							<input type="text" name="enama" id="enama" class="form-control" required="" style="text-transform: uppercase;">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Poli</label>
							<select id="epoli" class="form-control select">
								
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" id="btn-save" class="btn btn-primary"><span class="icon-address-book"></span> Simpan</button>
							<button type="button" id="btn-save" class="btn btn-warning" onclick="dispose();"><span class="icon-x"></span> Batal</button>
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>


@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	

	

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master.dokter.ajaxListDokter') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'nama_dokter', name: 'nama_dokter'},
            {data: 'id_poli', name: 'id_poli'},
            {data: 'action', name: 'action'}
        ]
	});

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$(window).on('load',function(){
		
		table.clear();
		table.draw();
	
	});

	$('#btn-add').click(function(e){
		e.preventDefault();
		ajaxPoli();

		$('#nid').val('');
		$('#nama').val('');
		$('#modal_add').modal('show');
	});

	$('#form-add').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-add').attr('action'),
	        data : {nid:$('#nid').val(),nama:$('#nama').val(),poli:$('#poli').val()},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
                 $.unblockUI();
                 table.clear();
                 table.draw();
	        },
	        error: function(response) {
	          $.unblockUI();
                alert(response.status,response.responseText);
	        }
	    });
	});


	$('#table-list').on('click','.eddok',function(){
		$('#enid').val($(this).data('id'));
		$('#enama').val($(this).data('nama'));
		ajaxPoliEd($(this).data('poli'));

		$('#modal_edit').modal('show');

	});


	$('#form-edit').submit(function(event){
		event.preventDefault();
		// console.log($('#enid').val(),$('#enama').val(),$('#epoli').val());
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  $('#form-edit').attr('action'),
	        data : {nid:$('#enid').val(),nama:$('#enama').val(),poli:$('#epoli').val()},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
                 $.unblockUI();
                 table.clear();
                 table.draw();
                 $('#modal_edit').modal('hide');
	        },
	        error: function(response) {
	          $.unblockUI();
                alert(response.status,response.responseText);
	        }
	    });
	});


	$('#table-list').on('click','.deldok',function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  "{{ route('master.dokter.deleteDokter') }}",
	        data : {nid:$(this).data('id')},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
                 $.unblockUI();
                 table.clear();
                 table.draw();
          
	        },
	        error: function(response) {
	          $.unblockUI();
                alert(response.status,response.responseText);
	        }
	    });

	});


});

function ajaxPoli(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url :  "{{ route('master.dokter.ajaxGetPoli') }}",
        success: function(response) {

         	var poli = response.poli;

         	$('#poli').empty();
         	$('#poli').append('<option value="">--Pilih Poli--</option>');

         	for (var i = 0; i < poli.length; i++) {
         		$('#poli').append('<option value="'+poli[i]['id']+'">'+poli[i]['nama_poli']+'</option>');
         	}
        },
        error: function(response) {
           console.log(response);
            
        }
    });
}

function ajaxPoliEd(idpol){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url :  "{{ route('master.dokter.ajaxGetPoli') }}",
        success: function(response) {

         	var poli = response.poli;

         	$('#epoli').empty();
         	$('#epoli').append('<option value="">--Pilih Poli--</option>');

         	for (var i = 0; i < poli.length; i++) {

         		if (poli[i]['id']==idpol) {
         			var selc = "selected";
         		}else{
         			var selc = "selected";
         		}

         		$('#epoli').append('<option value="'+poli[i]['id']+'" '+selc+'>'+poli[i]['nama_poli']+'</option>');
         	}
        },
        error: function(response) {
           console.log(response);
            
        }
    });
}






</script>
@endsection