@extends('layouts.app', ['active' => 'permissions'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Admin</a></li>
			<li class="active">Permissions</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class="panel panel-flat">
			<div class="panel-body">
				<div class="row form-group">
					<form action="{{ route('admin.addPermission') }}" id="form-add" >
						<div class="row">
							<label><b>Name</b></label>
							<input type="text" name="name" id="name" class="form-control" required="">
						</div>

						<div class="row">
							<label><b>Display Name</b></label>
							<input type="text" name="display" id="display" class="form-control" required="">
						</div>
						<div class="row">
							<label><b>Description</b></label>
							<input type="text" name="descpt" id="descpt" class="form-control" required="">
						</div>
						<div class="row">
							<button type="submit" class="btn btn-success">Simpan</button>
						</div>
					</form>
					
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Display</th>
									<th>Description</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('admin.getPermission') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'display_name', name: 'display_name'},
            {data: 'description', name: 'description', sortable: false, orderable: false, searchable: false}
        ]
	});


	$('#form-add').submit(function(event){
		event.preventDefault();

		
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-add').attr('action'),
	        data:{name:$('#name').val(),display:$('#display').val(),descript:$('#descpt').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
	        	
	        	var notif = response.data;
                alert(notif.status,notif.output);
                
               
                window.location.reload();
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});
});
</script>
@endsection

