<ul class="icons-list">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="icon-menu9"></i>
		</a>

		<ul class="dropdown-menu dropdown-menu-right">
			
			@if(isset($edituser))
			<li><a href="{!! $edituser !!}"><i class="icon-paragraph-justify2"></i> Edit User</a></li>
			@endif

			@if(isset($innactive))
			<li><a href="{!! $innactive !!}"><i class="icon-x"  class="ignore-click innactive"></i> Innactive User</a></li>
			@endif

			@if(isset($editrole))
			<li><a href="{!! $editrole !!}"><i class="icon-paragraph-justify2"  class="ignore-click"></i> Edit Role</a></li>
			@endif

			@if(isset($editpasien))
			<li><a href="#" data-id="{!! $editpasien !!}" class=" ignore-click editpasien"><i class="icon-pencil3"></i> Edit Pasien</a></li>
			@endif

			@if(isset($regist))
			<li><a href="#" data-id="{!! $regist !!}" class=" ignore-click regist"><i class=" icon-quill4"></i> Registrasi Periksa</a></li>
			@endif

			@if(isset($riwayat))
			<li><a href="#" data-id="{!! $riwayat !!}" class=" ignore-click riwayat"><i class="icon-file-text3"></i> Riwayat Periksa</a></li>
			@endif

			@if(isset($edProduct))
			<li><a href="#" data-id="{!! $edProduct !!}" class=" ignore-click edProduct"><i class="icon-pencil3"></i> Edit</a></li>
			@endif

			@if(isset($delProduct))
			<li><a href="#" data-id="{!! $delProduct !!}" class=" ignore-click delProduct"><i class="icon-trash-alt"></i> Delete</a></li>
			@endif

			@if(isset($listrm))
			<li><a href="#" data-id="{!! $listrm !!}" class=" ignore-click listrm" onclick="viewList(this);"><i class="icon-paragraph-justify2"></i> List RM</a></li>
			@endif

			@if(isset($edKec))
			<li><a href="#" data-id="{!! $edKec['id'] !!}" data-nama="{!! $edKec['nama'] !!}" class=" ignore-click edKec"><i class="icon-pencil7"></i> Edit</a></li>
			@endif

			@if(isset($delKec))
			<li><a href="#" data-id="{!! $delKec !!}" class=" ignore-click delKec"><i class="icon-trash"></i> Delete</a></li>
			@endif

			@if(isset($edKel))
			<li><a href="#" data-id="{!! $edKel['id'] !!}" data-nama="{!! $edKel['namakel'] !!}" data-kec="{!! $edKel['idkec'] !!}" class=" ignore-click edKel"><i class="icon-pencil7"></i> Edit</a></li>
			@endif

			@if(isset($delKel))
			<li><a href="#" data-id="{!! $delKel !!}" class=" ignore-click delKel"><i class="icon-trash"></i> Delete</a></li>
			@endif

			@if(isset($edpol))
			<li><a href="#" data-id="{!! $edpol['id'] !!}" data-nama="{!! $edpol['nama'] !!}"  class=" ignore-click edpol"><i class="icon-pencil7"></i> Edit</a></li>
			@endif

			@if(isset($delpoli))
			<li><a href="#" data-id="{!! $delpoli !!}" class=" ignore-click delpoli"><i class="icon-trash"></i> Delete</a></li>
			@endif

			@if(isset($eddok))
			<li><a href="#" data-id="{!! $eddok['id'] !!}" data-nama="{!! $eddok['nama'] !!}" data-poli="{!! $eddok['poli'] !!}"  class=" ignore-click eddok"><i class="icon-pencil7"></i> Edit</a></li>
			@endif

			@if(isset($deldok))
			<li><a href="#" data-id="{!! $deldok['id'] !!}"   class=" ignore-click deldok"><i class="icon-trash"></i> Delete</a></li>
			@endif

			@if(isset($edktg))
			<li><a href="#" data-id="{!! $edktg['id'] !!}" data-nama="{!! $edktg['nama'] !!}" data-type="{!! $edktg['type'] !!}"  class=" ignore-click edktg"><i class="icon-pencil7"></i> Edit</a></li>
			@endif

			@if(isset($delktg))
			<li><a href="#" data-id="{!! $delktg['id'] !!}"   class=" ignore-click delktg"><i class="icon-trash"></i> Delete</a></li>
			@endif

			@if(isset($edsat))
			<li><a href="#" data-id="{!! $edsat['id'] !!}" data-nama="{!! $edsat['nama'] !!}"  class=" ignore-click edsat"><i class="icon-pencil7"></i> Edit</a></li>
			@endif

			@if(isset($delsat))
			<li><a href="#" data-id="{!! $delsat['id'] !!}"   class=" ignore-click delsat"><i class="icon-trash"></i> Delete</a></li>
			@endif

			@if(isset($printNota))
			<li><a href="{!! $printNota !!}"  target="_blank" class=" ignore-click"><i class="icon-trash"></i> Print Nota</a></li>
			@endif
			
		</ul>
	</li>
</ul>