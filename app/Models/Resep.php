<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Resep extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'resep';
	protected $fillable = ['id_pemeriksaan','petugas','created_at','updated_at','deleted_at'];
}
