<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class HasilPeriksa extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $table = 'hasil_periksa';
    protected $fillable = ['id_pemeriksaan','pemeriksaan_fisik','diagnosis','tindakan','created_at','updated_at','deleted_at','tanggal_periksa','dokter_periksa_id'];
}
