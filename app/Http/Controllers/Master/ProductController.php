<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;
use App\Models\Kategory;
use App\Models\Satuan;
use App\Models\Product;

class ProductController extends Controller
{
    public function kategSatuan(){
    	return view('master.product.kategory_satuan');
    }

    public function ajaxGetSatuan(){
    	$data = Satuan::whereNull('deleted_at')->orderBy('created_at','desc');

    	return DataTables::of($data)
    						->addColumn('action',function($data){
    							return view('_action',[
									'edsat'=>['id'=>$data->id,'nama'=>$data->nama_satuan],
									'delsat'=>['id'=>$data->id],
								]);
    						})
    						->rawColumns(['action'])
    						->make(true);
    }

    public function addSatuan(Request $request){
    	try {
			DB::begintransaction();
			$in = array(
				'nama_satuan'=>strtoupper(trim($request->nama_satuan))
			);

			Satuan::firstOrCreate($in);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Tambah Satuan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Satuan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function editSatuan(Request $request){
    	try {
			DB::begintransaction();
			$up = array(
				'nama_satuan'=>strtoupper(trim($request->nama_satuan)),
				'updated_at'=>Carbon::now()
			);

			Satuan::where('id',$request->id)->update($up);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Edit Satuan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Edit Satuan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function deleteSatuan(Request $request){

    	
    	try {
			DB::begintransaction();
			$del = array(
				'deleted_at'=>Carbon::now()
			);

			Satuan::where('id',$request->id)->update($del);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Hapus Satuan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Hapus Satuan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function ajaxGetKategory(){
    	$data = Kategory::whereNull('deleted_at')->orderBy('created_at','desc');

    	return DataTables::of($data)
    						->addColumn('action',function($data){
    							return view('_action',[
									'edktg'=>['id'=>$data->id,'nama'=>$data->nama_kategory,'type'=>$data->type],
									'delktg'=>['id'=>$data->id],
								]);
    						})
    						->rawColumns(['action'])
    						->make(true);
    }

    public function addKategory(Request $request){
    	try {
			DB::begintransaction();
			$in = array(
				'nama_kategory'=>strtoupper(trim($request->nama_kategory)),
				'type'=>$request->type
			);

			Kategory::firstOrCreate($in);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Tambah Kategory Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Kategory Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function editKategory(Request $request){

    	
    	try {
			DB::begintransaction();
			$up = array(
				'nama_kategory'=>strtoupper(trim($request->nama_kategory)),
				'type'=>$request->type,
				'updated_at'=>Carbon::now()
			);

			Kategory::where('id',$request->id)->update($up);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Update Kategory Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Update Kategory Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function deleteKategory(Request $request){
    	try {
			DB::begintransaction();
			$del = array(
				'deleted_at'=>Carbon::now()
			);

			Kategory::where('id',$request->id)->update($del);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Hapus Kategory Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Hapus Kategory Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function product(){
    	return view('master.product.product');
    }

    public function ajaxGetProduct(){
    	$data =	DB::table('product')
    						->join('satuan','product.id_satuan','=','satuan.id')
    						->join('kategory','product.id_kategory','=','kategory.id')
    						->whereNull('product.deleted_at')
    						->whereNull('satuan.deleted_at')
    						->whereNull('kategory.deleted_at')
    						->select('product.id','product.nama_product','product.harga','product.kode_product','satuan.nama_satuan','kategory.nama_kategory')
    						->orderBy('product.created_at','desc');

    	return DataTables($data)
    					->addColumn('action',function($data){
    						return view('_action',[
    							'model'=>$data,
    							'edProduct'=>$data->id,
    							'delProduct'=>$data->id
    						]);
    					})
    					->rawColumns(['action'])->make(true);
    }

    public function ajaxListKatgSat(){
    	$satuan = Satuan::whereNull('deleted_at')->get();
    	$kateg = kategory::whereNull('deleted_at')->get();

    	return response()->json(['satuan'=>$satuan,'kategory'=>$kateg],200);
    }

    public function addProduct(Request $request){
    	$nama = strtoupper(trim($request->nama));
    	$plu = trim($request->plu);
    	$harga = $request->harga;
    	$satuan = $request->satuan;
    	$kateg = $request->kateg;
    	$id = $this->getProducID();

    	try {
			DB::begintransaction();
			$in = array(
				'id'=>$id,
				'nama_product'=>$nama,
				'harga'=>$harga,
				'kode_product'=>$plu,
				'id_satuan'=>$satuan,
				'id_kategory'=>$kateg,
				'created_at'=>Carbon::now()
			);

			Product::firstOrCreate($in);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Tambah Product Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Product Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    private function getProducID(){
    	$last = Product::orderBy('created_at','desc')->max('id');
    	$str = Carbon::now()->format('ymd');
    	if ($last==null) {
    		$int = 0;
    	}else{
    		$int = (int) substr($last,6,5);
    	}

    	return $str.sprintf("%04s", $int+1);
    }

    public function getProduct(Request $request){
    	$data =	DB::table('product')
    						->join('satuan','product.id_satuan','=','satuan.id')
    						->join('kategory','product.id_kategory','=','kategory.id')
    						->whereNull('product.deleted_at')
    						->whereNull('satuan.deleted_at')
    						->whereNull('kategory.deleted_at')
    						->select('product.id','product.nama_product','product.harga','product.kode_product','satuan.nama_satuan','kategory.nama_kategory','product.id_satuan','product.id_kategory')
    						->first();
    	return response()->json(['data'=>$data],200);
    }

    public function editProduct(Request $request){
    	try {
			DB::begintransaction();
			$upt = array(
				'nama_product'=>strtoupper(trim($request->nama)),
				'harga'=>$request->harga,
				'kode_product'=>trim($request->plu),
				'id_satuan'=>$request->satuan,
				'id_kategory'=>$request->kateg,
				'updated_at'=>Carbon::now()
			);

			Product::where('id',$request->id)->update($upt);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Ubah Product Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Ubah Product Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function delProd(Request $request){
    	

    	try {
			DB::begintransaction();
			$del = array(
				'deleted_at'=>Carbon::now()
			);

			Product::where('id',$request->id)->update($del);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Hapus Product Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Hapus Product Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

}
