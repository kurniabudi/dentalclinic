<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;

class AlamatController extends Controller
{
    public function kecamatan(){
    	$kec = DB::table('kecamatan')->orderby('created_at','desc')->first();

    	if ($kec==null) {
    		$int = 0;
    	}else{
    		$int = (int) substr($kec->id, 3,3);
    	}

 
    	$new = 'KEC'.sprintf("%03s", $int+1);

    	return view('master.alamat.kecamatan')->with('idkec',$new);
    }

    public function ajaxGetKec(){
    	$data = DB::table('kecamatan')->whereNull('deleted_at')->orderby('id','desc');

    	return DataTables::of($data)
    						->addColumn('action',function($data){
    							return view('_action',[
    										'model'=>$data,
    										'edKec'=>['id'=>$data->id,'nama'=>$data->nama_kecamatan],
    										'delKec'=>$data->id
    									]);
    						})
    						->rawColumns(['action'])
    						->make('true');
    }

    public function editKec(Request $request){
    	$id = $request->id;
    	$nama = trim($request->nm_kec);
    	

    	try {
			DB::begintransaction();
			$up = array(
				'nama_kecamatan'=>strtoupper($nama),
				'updated_at'=>Carbon::now()
			);

			DB::table('kecamatan')->where('id',$id)->update($up);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Update Kecamatan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Update Kecamatan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);

    }

    public function delKec(Request $request){
    	$id = $request->id;

    	try {
			DB::begintransaction();
			$del = array(
				'deleted_at'=>Carbon::now()
			);

			DB::table('kecamatan')->where('id',$id)->update($del);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Delete Kecamatan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Delete Kecamatan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);

    }

    public function addKec(Request $request){
    	$id = $request->id;
    	$nama = strtoupper(trim($request->nm_kec));

    	try {
			DB::begintransaction();
			$in = array(
				'id'=>$id,
				'nama_kecamatan'=>$nama,
				'created_at'=>Carbon::now()
			);

			DB::table('kecamatan')->insert($in);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Tambah Kecamatan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Kecamatan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function kelurahan(){
    	$kel = DB::table('kelurahan')->orderby('created_at','desc')->first();
    	$kec = DB::table('kecamatan')->whereNull('deleted_at')->orderby('id','asc')->get();


    	if ($kel==null) {
    		$int = 0;
    	}else{
    		$int = (int) substr($kel->id, 3,3);
    	}

 
    	$new = 'KEL'.sprintf("%03s", $int+1);

    	return view('master.alamat.kelurahan')->with('idkel',$new)
    											->with('kec',$kec);
    }

    public function ajaxGetKel(){
    	$data = DB::table('kelurahan')
    					->join('kecamatan','kelurahan.id_kec','=','kecamatan.id')    						
    					->whereNull('kelurahan.deleted_at')
    					->whereNull('kecamatan.deleted_at')
    					->select('kelurahan.id','kelurahan.id_kec','kelurahan.nama_kelurahan')
    					->orderby('kelurahan.id','desc');

    	return DataTables::of($data)
    					->editColumn('id_kec',function($data){
    						$kec = DB::table('kecamatan')->where('id',$data->id_kec)->first();

    						return $kec->nama_kecamatan;
    					})
    					->addColumn('action',function($data){
    						return view('_action',[
    									'model'=>$data,
    									'edKel'=>['id'=>$data->id,'namakel'=>$data->nama_kelurahan,'idkec'=>$data->id_kec],
    									'delKel'=>$data->id
    								]);
    					})
    					->rawColumns(['id_kec','action'])
    					->make(true);
    }

    public function ajaxGetCombkec(){
    	$kec = DB::table('kecamatan')->whereNull('deleted_at')->get();

    	return response()->json(['data'=>$kec],200);
    }

    public function addKel(Request $request){
    	try {
			DB::begintransaction();
			$in = array(
				'id'=>$request->id,
				'nama_kelurahan'=>strtoupper(trim($request->nm_kel)),
				'id_kec'=>$request->id_kec,
				'created_at'=>Carbon::now()
			);

			DB::table('kelurahan')->insert($in);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Tambah Kelurahan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Kelurahan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function editKel(Request $request){
    	try {
			DB::begintransaction();
			$up = array(
				'nama_kelurahan'=>strtoupper(trim($request->nm_kel)),
				'id_kec'=>$request->id_kec,
				'updated_at'=>Carbon::now()
			);

			DB::table('kelurahan')->where('id',$request->id)->update($up);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Edit Kelurahan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Edit Kelurahan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    public function delKel(Request $request){
    	try {
			DB::begintransaction();
			$del = array(
				'deleted_at'=>Carbon::now()
			);

			DB::table('kelurahan')->where('id',$request->id)->update($del);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Edit Kelurahan Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Edit Kelurahan Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }
}
