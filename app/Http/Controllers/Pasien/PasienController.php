<?php
 
namespace App\Http\Controllers\Pasien;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;
use App\Models\Pasien;
use App\Models\Poli;
use App\Models\Dokter;
use App\Models\Periksa;
 
 
class PasienController extends Controller
{
    //list pasien
    public function listpasien(){
        return view ('pasien.list_pasien');
    } 

    public function getPasien(){
        $data = Pasien::whereNull('deleted_at')->orderBy('nama','asc');
        // $data = DB::table('pasien');

        

        return DataTables::of($data)
                        ->editColumn('nama',function($data){
                            if ($data->gender=='LAKI-LAKI') {
                                $jk = "(L)";
                            }else{
                                $jk = "(P)";
                            }

                            return $jk." ".$data->nama;
                        })
                        ->editColumn('tempat_lahir',function($data){
                            return $data->tempat_lahir.", ".date_format(date_create($data->tanggal_ahir),'d-m-Y');
                        })
                        ->addColumn('action',function($data){
                            return view('_action',[
                                            'model'=>$data,
                                            'editpasien'=>$data->user_id,
                                            'listrm'=>$data->user_id
                                        ]);
                        })
                        ->rawColumns(['tempat_lahir','action'])->make(true);
    }

    public function ajaxListKec(){
        $data  = DB::table('kecamatan')->whereNull('deleted_at')->orderBy('id','desc')->get();

        return response()->json(['kec'=>$data],200);
    }

    public function ajaxListKel(Request $request){

        $data  = DB::table('kelurahan')->where('id_kec',$request->idkec)->whereNull('deleted_at')->orderBy('id','desc')->get();

        return response()->json(['kel'=>$data],200);
    }

    public function addPasien(Request $request){
        $id = $this->userID();

        try {
            DB::begintransaction();
                $in =  array(
                    'user_id'=>$id,
                    'nama'=>strtoupper(trim($request->name)),
                    'nik'=>trim($request->nik),
                    'tempat_lahir'=>strtoupper(trim($request->tmp_lhr)),
                    'tanggal_lahir'=>date_format(date_create($request->tgl_lhr),'Y-m-d'),
                    'gender'=>$request->gender,
                    'agama'=>$request->agama,
                    'alamat'=>trim($request->alamat),
                    'no_hp'=>$request->nohp,
                    'id_kel'=>$request->idkel,
                    'created_at'=>Carbon::now()
                );

                Pasien::firstOrCreate($in);

            DB::commit();

                $data_response = [
                            'status' => 200,
                            'output' => 'Tambah Pasien Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Pasien Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

 
    private function userID(){
        // $now = Carbon::now()->format('Y-m-d');
        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->format('m');
        $tgl = Carbon::now()->format('Y-m-');

        $lastpas = Pasien::whereYear('created_at',$year)
                            ->whereMonth('created_at',$month)
                            ->max('user_id');

        

        if ($lastpas==null) {
            $inc = 0 ;
        }else{
            $inc = (int) substr($lastpas, 8, 6);
        }
      
        return $tgl.sprintf("%06s", $inc+1);
    }



   //pendafaran periksa
    public function ajaxGetPasienPoli(){
        $pasien = Pasien::whereNull('deleted_at')->orderBy('nama','asc')->get();

        $poli = Poli::whereNull('deleted_at')->get();


        return response()->json(['pasien'=>$pasien,'poli'=>$poli],200);
    }

    public function ajaxDetPasRm(Request $request){
        $norm = $request->norm;
   
        $data = DB::table('pasien')
                            ->join('kelurahan','pasien.id_kel','=','kelurahan.id')
                            ->join('kecamatan','kelurahan.id_kec','=','kecamatan.id')
                            ->where('pasien.user_id',$norm)
                            ->whereNull('pasien.deleted_at')
                            ->select('pasien.nik','pasien.user_id','pasien.nama','pasien.gender','pasien.tempat_lahir','pasien.tanggal_lahir','pasien.alamat','kelurahan.nama_kelurahan','kecamatan.nama_kecamatan')->first();

        return response()->json(['data'=>$data],200);
    }

    public function addPendaftaran(Request $request){
        $pmid =$this->daftarID();


        $cek = Periksa::where('user_id',$request->norm)->where('poli_id',$request->poli)->where('status','OPEN')->exists();
        if ($cek) {
            return response()->json("Pemeriksaan dengan nomor RM ".$request->norm." masih belum close !!",422);
        }
        try {
            DB::beginTransaction();

                $in = array(
                    'id'=>$pmid,
                    'user_id'=>$request->norm,
                    'poli_id'=>$request->poli,
                    'status'=>"OPEN",
                    'petugas'=>Auth::user()->id,
                    'dokter_id'=>$request->dokt,
                    'created_at'=>carbon::now()
                );

                Periksa::firstOrCreate($in);
            DB::commit();

                $data_response = [
                            'status' => 200,
                            'output' => 'Pendaftaran Periksa Sukses . . .',
                            'idpr'=>$pmid
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Pendaftaran Periksa Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }
    private function daftarID(){
        $now = Carbon::now()->format('Y-m-d');
        $dfort = Carbon::now()->format('Ymd');
        $lastid = Periksa::whereDate('created_at',$now)
                    ->max('id');


        if ($lastid==null) {
            $inc = 0 ;
        }else{
            $inc = (int) substr($lastid, 8, 4);
        }

        return $dfort.sprintf("%04s", $inc+1);
    }

    public function printPeriksa(Request $request){
        $noperiksa = $request->id;

        $data = DB::table('pemeriksaan')
                        ->join('poli','pemeriksaan.poli_id','=','poli.id')
                        ->where('pemeriksaan.id',$noperiksa)
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('poli.deleted_at')
                        ->select('pemeriksaan.id','pemeriksaan.poli_id','poli.nama_poli')->first();
        $size = array(0, 0, 189.00, 150.00);
        $name = "#".$noperiksa;
        $pdf = \PDF::loadView('template.pemeriksaan',['data'=>$data])->setPaper($size,"potrait");
        return $pdf->stream($name);
    } 

    public function ajaxRm(Request $request){
        $user_id = $request->user_id;

        $data = DB::table('pemeriksaan')
                        // ->join('hasil_periksa','pemeriksaan.id','=','hasil_periksa.id_pemeriksaan')
                        ->join('poli','pemeriksaan.poli_id','=','poli.id')
                        ->where('pemeriksaan.user_id',$user_id)
                        ->whereIn('pemeriksaan.status',['OPEN','CLOSE'])
                        ->whereNull('pemeriksaan.deleted_at')
                        // ->whereNull('hasil_periksa.deleted_at')
                        ->select('pemeriksaan.id','poli.nama_poli','pemeriksaan.status')
                        ->orderby('pemeriksaan.created_at','desc');

        return DataTables::of($data)
                            ->editColumn('id',function($data){

                                return $data->id." ( ".$data->nama_poli." )";


                            })
                            ->editColumn('status',function($data){
                                if ($data->status=="OPEN") {
                                    $selop = "selected";
                                    $selcl = "";
                                    $selcn = "";
                                    $disb = "";
                                }else if ($data->status=="CLOSE"){
                                    $selop = "";
                                    $selcl = "selected";
                                    $selcn = "";
                                    $disb = "disabled";
                                }else{
                                    $selop = "";
                                    $selcl = "";
                                    $selcn = "selected";
                                    $disb = "disabled";
                                }

                                return '<select class="form-control status" id="status" data-id ="'.$data->id.'" '.$disb.' onchange="changeStat(this);">
                                            <option value="OPEN"'.$selop.'>OPEN</option>
                                            <option value="CLOSE"'.$selcl.'>CLOSE</option>
                                            <option value="CANCEL"'.$selcn.'>CANCEL</option>
                                        </select>';
                            })
                            ->addColumn('hasil',function($data){
                               

                                return $this->getHasil($data->id);
                            })
                            ->addColumn('action',function($data){
                               

                                return view('_action',[
                                                'printNota'=>route('layanan.kasir.printNota',['id'=>$data->id])
                                            ]);
                            })
                            ->rawColumns(['id','status','hasil','action'])->make(true);
    }

    private function getHasil($noperiksa){
        $hasil = DB::Table('hasil_periksa')->join('dokter','hasil_periksa.dokter_periksa_id','=','dokter.id')->where('hasil_periksa.id_pemeriksaan',$noperiksa)->whereNull('hasil_periksa.deleted_at')->select('hasil_periksa.tanggal_periksa','hasil_periksa.pemeriksaan_fisik','hasil_periksa.diagnosis','hasil_periksa.tindakan','hasil_periksa.dokter_periksa_id','dokter.nama_dokter')->get();
        $list ="";
        foreach ($hasil as $hsl) {
            $tgl = "<b>Tanggal : </b>".date_format(date_create($hsl->tanggal_periksa),'d-m-Y H:i:s')."<br>";
            $dok = "<b>Dokter Pemeriksaan : </b>".$hsl->nama_dokter."<br>";
            $fsk = "<b>Pemeriksaan Fisik : </b>".$hsl->pemeriksaan_fisik."<br>";
            $diag = "<b>Diagnosis : </b>".$hsl->diagnosis."<br>";
            $tind = "<b>Tindakan : </b>".$hsl->tindakan;

            $list = $list." ".$tgl." ".$dok." ".$fsk." ".$diag." ".$tind."<hr>";
        }

        return $list;
    }

    public function changeStatus(Request $request){
        $id = $request->id;
        $val = $request->val;

        try {
            DB::beginTransaction();

               Periksa::where('id',$id)->update(['status'=>$val]);
            DB::commit();

                $data_response = [
                            'status' => 200,
                            'output' => 'Ubah Status Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Ubah Status Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function ajaxDetPasien(Request $request){

        $id = $request->id;

        $data = DB::table('pasien')
                        ->join('kelurahan','pasien.id_kel','=','kelurahan.id')
                        ->join('kecamatan','kelurahan.id_kec','=','kecamatan.id')
                        ->where('pasien.user_id',$id)
                        ->whereNull('pasien.deleted_at')
                        ->select('pasien.user_id','pasien.nik','pasien.nama','pasien.tempat_lahir','pasien.tanggal_lahir','pasien.gender','pasien.agama','pasien.alamat','pasien.no_hp','pasien.id_kel','kelurahan.id_kec')->first();

        return response()->json(['data'=>$data],200);

    }

    public function editPasien(Request $request){
        $user_id = trim($request->user_id);
        

        try {
            DB::begintransaction();
                $up =  array(
                    'nama'=>strtoupper(trim($request->nama)),
                    'nik'=>trim($request->nik),
                    'tempat_lahir'=>strtoupper(trim($request->tempat_lahir)),
                    'tanggal_lahir'=>date_format(date_create($request->tanggal_lahir),'Y-m-d'),
                    'gender'=>$request->gender,
                    'agama'=>$request->agama,
                    'alamat'=>trim($request->alamat),
                    'no_hp'=>trim($request->nohp),
                    'id_kel'=>$request->idkel,
                    'created_at'=>Carbon::now()
                );

                Pasien::where('user_id',$user_id)->update($up);

            DB::commit();

                $data_response = [
                            'status' => 200,
                            'output' => 'Ubah Pasien Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Ubah Pasien Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function getDok(Request $request){

        $data = Dokter::where('id_poli',trim($request->idpoli))->whereNull('deleted_at')->get();


        return response()->json(['data'=>$data],200);
    }
} 
