<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;

use Rap2hpoutre\FastExcel\FastExcel;

use App\Models\Pembayaran;
use App\Models\Resep;
use App\Models\DetailResep;
use App\Models\Periksa;

class ReportController extends Controller
{
    public function reportTransaksi(){
        return view('report.transaksi');
    }

    public function ajaxGetTrans(Request $request){
        $date_range = explode('-', preg_replace('/\s+/', '', $request->tanggal));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

        $data = Pembayaran::whereNull('deleted_at')
                    ->whereBetween('created_at',[$from,$to])
                    ->orderBy('created_at','asc');

        return DataTables::of($data)
                            ->editColumn('created_at',function($data){
                                return Carbon::parse($data->created_at)->format('d/m/Y');
                            })
                            ->editColumn('id_pemeriksaan',function($data){
                                $pem = DB::Table('pemeriksaan')
                                            ->join('poli','poli.id','=','pemeriksaan.poli_id')
                                            ->whereNull('pemeriksaan.deleted_at')
                                            ->where('pemeriksaan.id',$data->id_pemeriksaan)
                                            ->first();

                                return $data->id_pemeriksaan." ".$pem->nama_poli;
                            })
                            ->addColumn('nama',function($data){
                                $nm = DB::Table('pemeriksaan')
                                                ->join('pasien','pasien.user_id','=','pemeriksaan.user_id')
                                                ->whereNull('pemeriksaan.deleted_at')
                                                ->where('pemeriksaan.id',$data->id_pemeriksaan)
                                                ->select('pasien.nama','pasien.nik','pasien.user_id')
                                                ->first();
                                return $nm->nama." (".$nm->nik.") ";

                            })
                            ->addColumn('total',function($data){
                                $tot = DB::Table('ns_pembayaran')
                                                ->where('pemeriksaan.id',$data->id_pemeriksaan)
                                                ->first();

                                return $tot->tot_bea;

                            })
                            ->addColumn('tot_bayar',function($data){
                                $byr = DB::Table('ns_pembayaran')
                                                ->where('pemeriksaan.id',$data->id_pemeriksaan)
                                                ->first();

                                return $byr->tot_bayar;

                            })
                            ->rawColumns(['id_pemeriksaan','nama','total','tot_bayar','created_at'])->make(true);
    }

    public function exportTrans(Request $request){
        $date_range = explode('-', preg_replace('/\s+/', '', $request->tanggal));
        $from = date_format(date_create($date_range[0]), 'Y-m-d 00:00:00');
        $to = date_format(date_create($date_range[1]), 'Y-m-d 23:59:59');

        $data = Pembayaran::whereNull('deleted_at')->whereBetween('created_at',[$from,$to])->orderBy('created_at','asc');
        $adfil = "";
        if (isset($request->filterby)) {
            $data = $data->where('id',$request->filterby)
                            ->orWhere('id_pemeriksaan',$request->filterby);
            $adfil = "_filterby_".$request->filterby;
        }

        $i = 1;
        $filename = "Report_transaksi_from_".$from."_to_".$to.$adfil;

        $data_result = $data->get();
        foreach ($data_result as $data_results) {
          $data_results->no=$i++;
        }

        if ($data_result->count()>0) {
          return (new FastExcel($data_result))->download($filename.'.xlsx',function($row) use($i){
                $nm = DB::Table('pemeriksaan')
                        ->join('pasien','pasien.user_id','=','pemeriksaan.user_id')
                        ->join('poli','poli.id','=','pemeriksaan.id_poli')
                        ->whereNull('pemeriksaan.deleted_at')
                        ->where('pemeriksaan.id',$data->id_pemeriksaan)
                        ->select('pasien.nama','pasien.nik','pasien.user_id','poli.nama_poli')
                        ->first();
                $trans = DB::table('ns_pembayaran')->where('id',$data->id_pemeriksaan)
                                ->first();
           
            return
              [
                '#'=>$row->no,
                'Tanggal'=>Carbon::Parse($row->created_at)->format('d/m/Y'),
                'No. Transaksi'=>$row->id,
                'No. Pemeriksaan'=>$row->id_pemeriksaan,
                'Nama'=>$nm->nama." (".$nm->nik.")",
                'No. RM'=>$nm->user_id,
                'Total Transaksi'=>$trans->tot_bea,
                'Total Bayar'=>$trans->tot_bayar
                

              ];
          });
       }else{
          return response()->json('no data', 422);
       }
    }
}
