<?php

namespace App\Http\Controllers\Layanan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;
use App\Models\Pembayaran;
use App\Models\Resep;
use App\Models\DetailResep;
use App\Models\Periksa;

class KasirController extends Controller
{
    private function kodeTrans(){
        $now = Carbon::now()->format('Y-m-d');
        $trans = Pembayaran::whereDate('created_at',$now)->orderBy('created_at','desc')->first();

        if ($trans==null) {
            $last = 0;
        }else{
            $last = (int) substr($trans->id,8);
        }

        $new = $now.sprintf("%06s", $last+1);

        return $new;
    }

    public function kasir(){
        $pasien = DB::table('pemeriksaan')
                        ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('pasien.deleted_at')
                        ->whereNotIn('pemeriksaan.status',['CANCEL'])
                        ->orderBy('pemeriksaan.status')
                        ->select('pemeriksaan.id','pasien.user_id','pasien.nama','pemeriksaan.status')->get();

        return view('layanan.kasir')->with('pasien',$pasien);
    }

    public function ajaxGetPeriksa(Request $request){
        $id = trim($request->noperiksa);

        $perik = DB::table('pemeriksaan')
                        ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                        ->leftjoin('dokter','pemeriksaan.dokter_id','=','dokter.id')
                        ->join('poli','pemeriksaan.poli_id','=','poli.id')
                        ->where('pemeriksaan.id',$id)
                        // ->where('pemeriksaan.status','OPEN')
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('pasien.deleted_at')
                        ->select('pasien.nik','pasien.nama','pasien.gender','pasien.tempat_lahir','pasien.tanggal_lahir','pemeriksaan.user_id','pemeriksaan.id','dokter.nama_dokter','poli.nama_poli','pemeriksaan.status')->first();

        // if ($perik->status!="OPEN") {
        //    return response()->json("Status periksa ".$perik->status." ! ! !",422);
        // }else{
            // $resep = DB::table('resep')
            //                     ->join('detail_resep','resep.id','=','detail_resep.id_resep')
            //                     ->join('product','detail_resep.id_product','=','product.id')
            //                     ->join('satuan','product.id_satuan','=','satuan.id')
            //                     ->where('resep.id_pemeriksaan',$id)
            //                     ->whereNull('resep.deleted_at')
            //                     ->whereNull('detail_resep.deleted_at')
            //                     ->select('product.id','product.kode_product','product.harga','product.nama_product','satuan.nama_satuan','detail_resep.qty','detail_resep.harga','detail_resep.sub_total',DB::raw('detail_resep.id as listid'))->get();
            $bayar = DB::table('ns_pembayaran')->where('id',$id)->first();

            return response()->json(['periksa'=>$perik,'bayar'=>$bayar],200);
        // }
       
        
    } 

    public function getResep(Request $request){
        $resep = DB::table('resep')
                                ->join('detail_resep','resep.id','=','detail_resep.id_resep')
                                ->join('product','detail_resep.id_product','=','product.id')
                                ->join('satuan','product.id_satuan','=','satuan.id')
                                ->where('resep.id_pemeriksaan',$request->id)
                                ->whereNull('resep.deleted_at')
                                ->whereNull('detail_resep.deleted_at')
                                ->select('product.id','product.kode_product','product.harga','product.nama_product','satuan.nama_satuan','detail_resep.qty','detail_resep.harga','detail_resep.sub_total',DB::raw('detail_resep.id as listid'))->get();
        return response()->json(['resep'=>$resep],200);
    }
    

    public function addPembayaran(Request $request){
        $noperiksa = $request->idperiksa;
        $total = $request->total;
        $bayar = $request->bayar;
        $kembali = $request->kembali;
        $diskon = $request->diskon;
        $kode = $this->kodeTrans();
        try {
            DB::begintransaction();
            if ($total<=($diskon+$bayar)) {
                $stat = "LUNAS";
                $stat_prk = "CLOSE";
            }else{
                $stat = "BELUM LUNAS";
                $stat_prk = "OPEN";
            }
            $bayar = array(
                'id'=>$kode,
                'status_pembayaran'=>$stat,
                'total'=>$total,
                'bayar'=>$bayar,
                'potongan'=>$diskon,
                'petugas'=>Auth::user()->id,
                'created_at'=>Carbon::now(),
                'id_pemeriksaan'=>$noperiksa
            );

            Pembayaran::firstOrCreate($bayar);
            Periksa::where('id',$noperiksa)->update(['status'=>$stat_prk,'status_pembayaran'=>$stat,'updated_at'=>Carbon::now()]);
            
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Simpan data Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Simpan data Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function listBea(){
        $data = DB::table('product')
                    ->join('kategory','product.id_kategory','=','kategory.id')
                    ->join('satuan','product.id_satuan','=','satuan.id')
                    ->where('kategory.type',"JASA")
                    ->whereNull('product.deleted_at')
                    ->whereNull('kategory.deleted_at')
                    ->whereNull('satuan.deleted_at')
                    ->select('product.id','product.nama_product','product.harga')->get();

        return response()->json(['listbea'=>$data],200);
    }

    public function addBeaPeriksa(Request $request){
        $idprod = explode("&",$request->id)[0];
        $harga = explode("&",$request->id)[1];
        $nopr = $request->noperiksa;

        $cekResep = Resep::where('id_pemeriksaan',$nopr)->whereNull('deleted_at')->first();

        try {
            DB::beginTransaction();
            if ($cekResep!=null) {
                // $cekprod = DetailResep::where('id_resep',$cekResep->id)->where('id_product',$idprod)->whereNull('deleted_at')->exists();
                // if ($cekprod) {
                //      return response()->json("Bea Periksa Sudah ada !!!",422);
                // }else{
                    $dtresp = array(
                            'id_resep'=>$cekResep->id,
                            'id_product'=>$idprod,
                            'qty'=>1,
                            'harga'=>$harga,
                            'sub_total'=>$harga,
                            'created_at'=>Carbon::now()
                        );
                   DetailResep::firstOrCreate($dtresp);
                // }
               
            }else{
                $resep = array(
                    'id_pemeriksaan'=>$nopr,
                    'petugas'=>Auth::user()->id,
                    'created_at'=>Carbon::now()
                );
                $inpem = Resep::firstOrCreate($resep);

                if ($inpem) {
                    $dtresp = array(
                            'id_resep'=>$inpem->id,
                            'id_product'=>$idprod,
                            'qty'=>1,
                            'harga'=>$harga,
                            'sub_total'=>$harga,
                            'created_at'=>Carbon::now()
                        );
                   DetailResep::firstOrCreate($dtresp);
                }
            }
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Simpan data Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Simpan data Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    public function printNota(Request $request){
        $id = $request->id;
       

        $header = DB::table('pemeriksaan')
                        ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                        ->leftjoin('dokter','pemeriksaan.dokter_id','=','dokter.id')
                        ->leftjoin('poli','pemeriksaan.poli_id','=','poli.id')
                        ->where('pemeriksaan.id',$id)
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('pasien.deleted_at')
                        ->select('pemeriksaan.id','pasien.user_id','pemeriksaan.created_at','pasien.nama','dokter.nama_dokter','poli.nama_poli')->first();

        $detail = DB::table('pemeriksaan')
                        ->join('resep','pemeriksaan.id','=','resep.id_pemeriksaan')
                        ->join('detail_resep','resep.id','=','detail_resep.id_resep')
                        ->join('product','detail_resep.id_product','=','product.id')
                        ->join('satuan','product.id_satuan','=','satuan.id')
                        ->where('pemeriksaan.id',$id)
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('resep.deleted_at')
                        ->whereNull('detail_resep.deleted_at')
                        ->select('resep.id_pemeriksaan','product.id','product.kode_product','product.nama_product','satuan.nama_satuan','detail_resep.harga','detail_resep.qty','detail_resep.sub_total')->get();
        $bayar = DB::table('pembayaran')->whereNull('deleted_at')->where('id_pemeriksaan',$id)->orderby('created_at','desc')->first();
        $total = DB::table('ns_pembayaran')->where('id',$id)->first();

        $size = array(0, 0, 831.50, 415.75);
        $name = "REG#".$id;
        $pdf = \PDF::loadView('template.nota',['head'=>$header,'detl'=>$detail,'pemby'=>$bayar,'total'=>$total])->setPaper("A4","potrait");
        return $pdf->stream($name);
    }

    public function deleteItem(Request $request){
        $id = $request->id;

        try {
            DB::beginTransaction();
                DB::table('detail_resep')->where('id',$id)->update(['deleted_at'=>carbon::now()]);
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Hapus data Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Hapus data Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }
}
