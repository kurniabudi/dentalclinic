<?php

namespace App\Http\Controllers\Layanan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;

use App\Models\DetailResep;
use App\Models\Periksa;
use App\Models\HasilPeriksa; 
use App\Models\Dokter; 

class PeriksaController extends Controller
{
    public function periksa(){
        $pasien = DB::table('pemeriksaan')
                        ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('pasien.deleted_at')
                        ->whereNotIn('pemeriksaan.status',['CLOSE','CANCEL'])
                        ->select('pemeriksaan.id','pasien.user_id','pasien.nama')->get();

        return view('layanan.periksa')->with('pasien',$pasien);
    }

    public function ajaxPeriksa(Request $request){
        $noperiksa = $request->noperiksa;

        $data = DB::table('pemeriksaan')
                    ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                    ->join('poli','pemeriksaan.poli_id','=','poli.id')
                    ->whereNull('pemeriksaan.deleted_at')
                    ->where('pemeriksaan.id',$noperiksa)
                    ->select('pasien.user_id','pasien.nama','pasien.nik','pasien.tanggal_lahir','pasien.gender','pemeriksaan.id','pemeriksaan.status','poli.nama_poli','pemeriksaan.status','pemeriksaan.dokter_id')
                    ->first();

        if(empty($data)){
             return response()->json("Nomor Periksa Tidak ditemukan",422);
        }else if($data->status!="OPEN"){
             return response()->json("Status periksa ".$data->status." ! ! !",422);    
        }else{
            $tlh = date_create($data->tanggal_lahir);
            $now = date_create(Carbon::now()->format('Y-m-d'));
            $diff = $now->diff($tlh);

           
            if ($data->gender=="LAKI-LAKI") {
                $gen = "(L)";
            }else{
                $gen = "(P)";
            }

            $pasien = array(
                'id'=>$data->id,
                'noperiksa'=>$data->id." (".$data->nama_poli.")",            
                'user_id'=>$data->user_id,
                'nama'=>$gen." ".$data->nama,
                'nik'=>$data->nik,
                'umur'=>$diff->y." Th. ".$diff->m." Bln. ".$diff->d." Hr.",
                'status'=>$data->status,
                'dokter_id'=>$data->dokter_id
            );


            return response()->json(['pasien'=>$pasien],200);
        }

        
    }

    public function ajaxRiwayat(Request $request){
        $norm = $request->norm;
      
        $data = DB::table('pemeriksaan')
                        ->join('poli','pemeriksaan.poli_id','=','poli.id')
                        ->Join('hasil_periksa','pemeriksaan.id','=','hasil_periksa.id_pemeriksaan')
                        ->join('dokter','hasil_periksa.dokter_periksa_id','=','dokter.id')
                        ->where('pemeriksaan.user_id',$norm)
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('hasil_periksa.deleted_at')
                        ->select('poli.nama_poli','hasil_periksa.tanggal_periksa','hasil_periksa.pemeriksaan_fisik','hasil_periksa.diagnosis','hasil_periksa.tindakan','hasil_periksa.id','dokter.nama_dokter',DB::raw('pemeriksaan.id as pm_id'))
                        ->orderby('hasil_periksa.tanggal_periksa','desc');

        // dd($data->get());
        
        return DataTables::of($data)
                            ->addColumn('tanggal_periksa',function($data){
                                $tgl = "<b>Tanggal : </b>".date_format(date_create($data->tanggal_periksa),'d-m-Y H:i:s')."<br>";
                                $dok = "<b>Dokter Pemeriksaan : </b>".$data->nama_dokter."<br>";
                                $poli = "<b>Poli : </b>".$data->nama_poli;

                                return $tgl." ".$dok." ".$poli;
                            })
                            ->editColumn('id',function($data){
                                return "<b>".$data->pm_id."</b>";
                            })
                            ->rawColumns(['tanggal_periksa','id'])->make(true);
    }

    public function ajaxListDokter(){
        $data = Dokter::whereNull('deleted_at')->orderBy('nama_dokter','asc')->get();

        return response()->json(['ldok'=>$data],200);
    }
   

    public function addPeriksa(Request $request){
        $nopr = $request->tmperiksa;
        $pemr_fisik = trim($request->pemr_fisik);
        $diagnosis = trim($request->diagnosis);
        $tindakan = trim($request->tindakan);

        $tgl = Carbon::parse(date_create($request->tgl))->format('Y-m-d');
        $jam = Carbon::parse($request->jam)->format('H:i:s');
        $dokid = trim($request->dokid);

        $tgl_periksa = date_create($tgl." ".$jam);


      


        try {
            DB::begintransaction();
                $detail = array(
                    'id_pemeriksaan'=>$nopr,
                    'pemeriksaan_fisik'=>$pemr_fisik,
                    'diagnosis'=>$diagnosis,
                    'tindakan'=>$tindakan,
                    'tanggal_periksa'=>$tgl_periksa,
                    'dokter_periksa_id'=>$dokid,
                    'created_at'=>carbon::now()
                );

           
                HasilPeriksa::firstOrCreate($detail);

                Periksa::where('id',$nopr)->update(['updated_at'=>Carbon::now()]);
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Simpan data Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Simpan data Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    
}
