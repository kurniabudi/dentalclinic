-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2021 at 08:05 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klinik`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_resep`
--

CREATE TABLE `detail_resep` (
  `id` char(36) NOT NULL,
  `id_resep` char(36) DEFAULT NULL,
  `id_product` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_resep`
--

INSERT INTO `detail_resep` (`id`, `id_resep`, `id_product`, `qty`, `harga`, `sub_total`, `created_at`, `updated_at`, `deleted_at`) VALUES
('6106b8e0-cc57-11eb-b820-654dbc83df1b', '8b64ead0-cc54-11eb-9f3b-9193f640f3d7', '2106060003', 1, 30000, 30000, '2021-06-13 14:55:22', '2021-06-13 14:55:22', NULL),
('7c5478c0-cc55-11eb-ad7b-c753bf92907a', '8b64ead0-cc54-11eb-9f3b-9193f640f3d7', '2106060003', 1, 30000, 30000, '2021-06-13 14:41:49', '2021-06-13 14:41:49', NULL),
('8b654ed0-cc54-11eb-8b80-41d92f982376', '8b64ead0-cc54-11eb-9f3b-9193f640f3d7', '2105260001', 1, 50001, 50001, '2021-06-13 14:35:05', '2021-06-13 14:35:05', NULL),
('91856a40-e84b-11eb-aad8-ab2a6ab14b32', '8b64ead0-cc54-11eb-9f3b-9193f640f3d7', '2106060003', 1, 30000, 30000, '2021-07-19 04:41:22', '2021-07-19 04:41:22', NULL),
('961c6aa0-e7d4-11eb-961c-5199e9f5decf', '8b64ead0-cc54-11eb-9f3b-9193f640f3d7', '2106060003', 1, 30000, 30000, '2021-07-18 14:29:40', '2021-07-18 14:29:40', NULL),
('e0e67080-e7c9-11eb-9997-3fc26679a054', '8b64ead0-cc54-11eb-9f3b-9193f640f3d7', '2106060003', 1, 30000, 30000, '2021-07-18 13:13:01', '2021-07-18 13:13:01', '2021-07-18 13:13:03'),
('ed609920-f919-11eb-ac2e-ef2180a66f83', 'ed5e0380-f919-11eb-9e5f-653aac3a4e18', '2105260002', 1, 6000, 6000, '2021-08-09 13:58:51', '2021-08-09 13:58:51', NULL),
('f72fe970-f919-11eb-a921-894af9dfd9ac', 'ed5e0380-f919-11eb-9e5f-653aac3a4e18', '2106060003', 1, 30000, 30000, '2021-08-09 13:59:08', '2021-08-09 13:59:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id` varchar(255) NOT NULL,
  `nama_dokter` varchar(255) DEFAULT NULL,
  `id_poli` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id`, `nama_dokter`, `id_poli`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
('1234567', 'NAUFAL SETIAWA', 'POL-001', 1, '2021-05-23 14:37:36', '2021-05-23 14:37:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hasil_periksa`
--

CREATE TABLE `hasil_periksa` (
  `id` char(36) NOT NULL,
  `id_pemeriksaan` char(36) DEFAULT NULL,
  `pemeriksaan_fisik` varchar(255) DEFAULT NULL,
  `diagnosis` varchar(255) DEFAULT NULL,
  `tindakan` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tanggal_periksa` timestamp NULL DEFAULT NULL,
  `dokter_periksa_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_periksa`
--

INSERT INTO `hasil_periksa` (`id`, `id_pemeriksaan`, `pemeriksaan_fisik`, `diagnosis`, `tindakan`, `created_at`, `updated_at`, `deleted_at`, `tanggal_periksa`, `dokter_periksa_id`) VALUES
('d9cdaa30-fded-11eb-8c4b-8b67f1f077db', '202107310001', 'asdasasda', 'asdada', 'adsadad', '2021-08-15 17:25:56', '2021-08-15 17:25:56', NULL, '2021-08-15 17:25:00', '1234567');

-- --------------------------------------------------------

--
-- Table structure for table `kategory`
--

CREATE TABLE `kategory` (
  `id` int(11) NOT NULL,
  `nama_kategory` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategory`
--

INSERT INTO `kategory` (`id`, `nama_kategory`, `created_at`, `updated_at`, `deleted_at`, `type`) VALUES
(1, 'OBAT BATUK', '2021-05-23 19:16:05', NULL, NULL, 'BARANG'),
(2, 'OBAT KULIT', '2021-05-25 21:45:14', '2021-05-25 21:45:14', NULL, 'BARANG'),
(3, 'OBAT GATAL', '2021-06-01 16:02:39', '2021-06-01 16:02:39', NULL, 'BARANG'),
(4, 'POLI GIGI', '2021-06-04 15:15:51', '2021-06-04 15:15:51', NULL, 'JASA');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` varchar(255) NOT NULL,
  `nama_kecamatan` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `nama_kecamatan`, `created_at`, `updated_at`, `deleted_at`) VALUES
('KEC001', 'KENDALI', '2021-05-23 13:05:29', '2021-06-26 13:12:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` varchar(255) NOT NULL,
  `id_kec` varchar(255) DEFAULT NULL,
  `nama_kelurahan` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `id_kec`, `nama_kelurahan`, `created_at`, `updated_at`, `deleted_at`) VALUES
('KEL001', 'KEC001', 'JAMBER', '2021-05-23 13:38:19', '2021-06-27 12:40:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ns_pembayaran`
-- (See below for the actual view)
--
CREATE TABLE `ns_pembayaran` (
`id` char(36)
,`status` varchar(255)
,`tot_bea` decimal(32,0)
,`tot_disk` decimal(41,0)
,`tot_bayar` decimal(41,0)
,`tot_old` decimal(41,0)
,`kurang_byr` decimal(43,0)
,`kurang_bea` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ns_sumbayar`
-- (See below for the actual view)
--
CREATE TABLE `ns_sumbayar` (
`id_pemeriksaan` char(36)
,`totbiaya` decimal(42,0)
,`totbayar` decimal(41,0)
,`kurang` decimal(43,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `user_id` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `id_kel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`user_id`, `nama`, `nik`, `tempat_lahir`, `tanggal_lahir`, `gender`, `agama`, `alamat`, `no_hp`, `created_at`, `updated_at`, `deleted_at`, `id_kel`) VALUES
('2021-05-000001', 'NAUFAL', '22222222', 'SEMARANG', '1994-12-09', 'LAKI-LAKI', 'ISLAM', 'KENDAL', '00000', '2021-07-17 08:57:20', '2021-05-21 23:52:43', NULL, 'KEL001'),
('2021-06-000001', 'CAHYO', '1231321313', 'KUTAI', '2021-06-28', 'PEREMPUAN', 'BUDHA', 'asdadadadasd1', '1213231231', '2021-06-29 15:18:39', '2021-06-29 15:18:39', NULL, 'KEL001');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` varchar(255) NOT NULL,
  `status_pembayaran` varchar(255) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `bayar` bigint(20) DEFAULT NULL,
  `kurang` bigint(20) DEFAULT NULL,
  `potongan` bigint(20) DEFAULT NULL,
  `petugas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `id_pemeriksaan` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `status_pembayaran`, `total`, `bayar`, `kurang`, `potongan`, `petugas`, `created_at`, `updated_at`, `deleted_at`, `id_pemeriksaan`) VALUES
('2021-08-09000001', 'LUNAS', 36000, 32400, NULL, 3600, 1, '2021-08-09 14:10:19', '2021-08-09 14:10:19', NULL, '202107310001');

-- --------------------------------------------------------

--
-- Table structure for table `pemeriksaan`
--

CREATE TABLE `pemeriksaan` (
  `id` char(36) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `poli_id` varchar(255) DEFAULT NULL,
  `dokter_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `petugas` int(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status_pembayaran` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemeriksaan`
--

INSERT INTO `pemeriksaan` (`id`, `user_id`, `poli_id`, `dokter_id`, `status`, `petugas`, `created_at`, `updated_at`, `deleted_at`, `remark`, `status_pembayaran`) VALUES
('202107310001', '2021-05-000001', 'POL-001', 1234567, 'OPEN', 1, '2021-07-31 15:20:45', '2021-08-15 17:25:56', NULL, NULL, 'LUNAS');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `updated_at` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'menu-dashboard', 'Menu Dashboard', 'Menu Dashboard', '2021-05-09 13:36:26.000000', '2021-05-09 13:36:26.000000'),
(2, 'menu-user-management', 'Menu User Management', 'Menu User Management', '2021-05-09 13:36:26.000000', '2021-05-09 13:36:26.000000'),
(3, 'menu-pendaftaran-pasien', 'Menu Pendaftaran Pasien', 'Menu Pendaftaran Pasien', '2021-05-17 00:13:37.000000', NULL),
(4, 'menu-master-data', 'Menu Master Data', 'Menu Master Data', '2021-05-23 09:42:22.000000', NULL),
(5, 'menu-pelayanan-pasien', 'Menu Pelayanan Pasien', 'Menu Pelayanan Pasien', '2021-05-23 19:27:04.000000', NULL),
(6, 'menu-pemeriksaan-dokter', 'Menu Pemeriksaan Dokter', 'Menu Pemeriksaan Dokter', '2021-06-03 14:49:18.000000', NULL),
(7, 'menu-coba', 'MENU COBA', 'MENU COBA', '2021-08-04 15:01:48.000000', NULL),
(8, 'menu-report', 'MENU REPORT', 'MENU REPORT', '2021-08-05 14:10:26.000000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(255) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 4),
(6, 4),
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `poli`
--

CREATE TABLE `poli` (
  `id` varchar(255) NOT NULL,
  `nama_poli` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poli`
--

INSERT INTO `poli` (`id`, `nama_poli`, `created_at`, `updated_at`, `deleted_at`) VALUES
('POL-001', 'GIGIRAVI', '2021-05-23 10:43:21', '2021-06-27 13:16:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` varchar(255) NOT NULL,
  `nama_product` varchar(255) DEFAULT NULL,
  `harga` bigint(20) DEFAULT NULL,
  `kode_product` varchar(255) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `id_kategory` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `nama_product`, `harga`, `kode_product`, `id_satuan`, `id_kategory`, `created_at`, `updated_at`, `deleted_at`) VALUES
('2105260001', 'KALPANAX CARI 15ML', 50001, '67021312567575', 1, '2', '2021-05-25 23:11:49', '2021-06-29 15:23:31', NULL),
('2105260002', 'ORALIT', 6000, '45642246', 2, '1', '2021-05-25 23:12:49', '2021-06-27 12:48:20', NULL),
('2106060003', 'PERIKSA GIGI', 30000, '123457', 4, '4', '2021-06-05 17:08:56', '2021-06-05 17:08:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resep`
--

CREATE TABLE `resep` (
  `id` char(36) NOT NULL,
  `id_pemeriksaan` char(36) DEFAULT NULL,
  `petugas` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resep`
--

INSERT INTO `resep` (`id`, `id_pemeriksaan`, `petugas`, `created_at`, `updated_at`, `deleted_at`) VALUES
('8b64ead0-cc54-11eb-9f3b-9193f640f3d7', '202106130001', 1, '2021-06-13 14:35:05', '2021-06-13 14:35:05', NULL),
('ed5e0380-f919-11eb-9e5f-653aac3a4e18', '202107310001', 1, '2021-08-09 13:58:51', '2021-08-09 13:58:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(3, 'ICT Admin', 'ICT Admin', 'ICT Admin', '2021-05-09 13:38:15', '2021-05-09 13:38:15'),
(4, 'DOKTER', 'DOKTER', 'DOKTER', '2021-08-01 13:46:49', '2021-08-01 13:46:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama_satuan` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `nama_satuan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BOTOL', '2021-05-23 19:16:43', NULL, NULL),
(2, 'PCS', '2021-05-25 21:37:14', '2021-05-25 21:37:14', NULL),
(3, 'VIAL', '2021-06-01 16:02:04', '2021-06-01 16:02:04', NULL),
(4, '-', '2021-06-05 17:08:25', '2021-06-05 17:08:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nik`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'Admin ICT', 'admin@klinik.co.id', '2021-05-09 13:40:38', '$2y$10$X5u3kBTzDC5dkeHD6C/fGOhq4lPb0WUvkuAar5cCnO9vESZA69y96', NULL, 'super_admin', '2021-05-09 20:40:38', '2021-05-09 20:40:38', NULL);

-- --------------------------------------------------------

--
-- Structure for view `ns_pembayaran`
--
DROP TABLE IF EXISTS `ns_pembayaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ns_pembayaran`  AS  select `pm`.`id` AS `id`,`pm`.`status_pembayaran` AS `status`,`bea`.`total` AS `tot_bea`,`pem`.`disk` AS `tot_disk`,`pem`.`bayar` AS `tot_bayar`,`pem`.`tot` AS `tot_old`,case when coalesce(`pem`.`bayar`,0) + coalesce(`pem`.`disk`,0) - `bea`.`total` >= 0 then 0 when coalesce(`pem`.`bayar`,0) + coalesce(`pem`.`disk`,0) - `bea`.`total` < 0 then abs(coalesce(`pem`.`bayar`,0) + coalesce(`pem`.`disk`,0) - `bea`.`total`) end AS `kurang_byr`,abs(coalesce(`pem`.`tot`,0) - `bea`.`total`) AS `kurang_bea` from ((`pemeriksaan` `pm` left join (select `rsp`.`id_pemeriksaan` AS `id_pemeriksaan`,sum(`drs`.`sub_total`) AS `total` from (`resep` `rsp` join `detail_resep` `drs` on(`rsp`.`id` = `drs`.`id_resep`)) where `rsp`.`deleted_at` is null and `drs`.`deleted_at` is null group by `rsp`.`id_pemeriksaan`) `bea` on(`pm`.`id` = `bea`.`id_pemeriksaan`)) left join (select `pembayaran`.`id_pemeriksaan` AS `id_pemeriksaan`,sum(`pembayaran`.`bayar`) AS `bayar`,sum(`pembayaran`.`potongan`) AS `disk`,sum(`pembayaran`.`total`) AS `tot` from `pembayaran` where `pembayaran`.`deleted_at` is null group by `pembayaran`.`id_pemeriksaan`) `pem` on(`pm`.`id` = `pem`.`id_pemeriksaan`)) where `pm`.`deleted_at` is null order by `pm`.`id` desc ;

-- --------------------------------------------------------

--
-- Structure for view `ns_sumbayar`
--
DROP TABLE IF EXISTS `ns_sumbayar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ns_sumbayar`  AS  select `pembayaran`.`id_pemeriksaan` AS `id_pemeriksaan`,sum(`pembayaran`.`total`) + sum(`pembayaran`.`kurang`) AS `totbiaya`,sum(`pembayaran`.`bayar`) AS `totbayar`,sum(`pembayaran`.`total`) + sum(`pembayaran`.`kurang`) - sum(`pembayaran`.`bayar`) AS `kurang` from `pembayaran` where `pembayaran`.`deleted_at` is null group by `pembayaran`.`id_pemeriksaan` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_resep`
--
ALTER TABLE `detail_resep`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_periksa`
--
ALTER TABLE `hasil_periksa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategory`
--
ALTER TABLE `kategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poli`
--
ALTER TABLE `poli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resep`
--
ALTER TABLE `resep`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategory`
--
ALTER TABLE `kategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
