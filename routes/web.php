<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
    	return redirect()->route('home.index');
    }

    return redirect('/login');
});

Route::get('/auth', 'Auth\LoginController@showLogin')->name('auth.showLogin');
Route::post('/auth/loginaction', 'Auth\LoginController@loginAction')->name('auth.loginAction');
Route::post('/auth/logoutaction', 'Auth\LoginController@logoutAction')->name('auth.logoutAction');

Auth::routes();

Route::middleware('auth')->group(function(){
	Route::prefix('/dashboard')->group(function(){
		Route::get('', 'DashboardController@index')->name('home.index');
		Route::get('/ajaxGetPeriksa', 'DashboardController@ajaxGetPeriksa')->name('home.ajaxGetPeriksa');
		Route::post('/editStatus', 'DashboardController@editStatus')->name('home.editStatus');
	});
	

	//account setting
	Route::prefix('/user')->group(function(){
		Route::get('/myAccount', 'User\AccountController@myAccount')->name('account.myAccount');
		Route::post('/myAccount/edit', 'User\AccountController@editPassword')->name('account.editPassword');
	});

	Route::prefix('/admin')->middleware(['permission:menu-user-management'])->group(function(){
		//user account
		Route::get('/user-account', 'Admin\AdminController@user_account')->name('admin.user_account');
		Route::get('/user-account/get-data', 'Admin\AdminController@getDataUser')->name('admin.getDataUser');
		Route::get('/user-account/ajaxGetRole', 'Admin\AdminController@ajaxGetRole')->name('admin.ajaxGetRole');
		Route::get('/user-account/add-user', 'Admin\AdminController@addUser')->name('admin.addUser');
		Route::get('/user-account/edit', 'Admin\AdminController@formEditUser')->name('admin.formEditUser');
		Route::get('/user-account/edit/edit-user', 'Admin\AdminController@editAccount')->name('admin.editAccount');
		Route::get('/user-account/edit/reset-password', 'Admin\AdminController@passwordReset')->name('admin.passwordReset');
		Route::get('/user-account/innactive', 'Admin\AdminController@innactiveUser')->name('admin.innactiveUser');
		Route::get('/user-account/factory', 'Admin\AdminController@getFactory')->name('admin.getFactory');

		//role
		Route::get('/role', 'Admin\AdminController@formRole')->name('admin.formRole');
		Route::get('/role/get-data', 'Admin\AdminController@ajaxRoleData')->name('admin.ajaxRoleData');
		Route::get('/role/new-role', 'Admin\AdminController@newRole')->name('admin.newRole');
		Route::post('/role/add-role', 'Admin\AdminController@addRole')->name('admin.addRole');
		Route::get('/role/edit', 'Admin\AdminController@editRoleUser')->name('admin.editRoleUser');
		Route::post('/role/update-role', 'Admin\AdminController@updateRole')->name('admin.updateRole');

		//permissions
		Route::get('/permission', 'Admin\AdminController@permission')->name('admin.permission');
		Route::get('/permission/get-data', 'Admin\AdminController@getPermission')->name('admin.getPermission');
		Route::post('/permission/addPermission', 'Admin\AdminController@addPermission')->name('admin.addPermission');
	});


	Route::prefix('/pasien')->middleware(['permission:menu-pendaftaran-pasien'])->group(function(){
		Route::get('/list', 'Pasien\PasienController@listpasien')->name('pasien.listpasien');
		Route::get('/list/get-data', 'Pasien\PasienController@getPasien')->name('pasien.getPasien');
		Route::get('/list/ajaxListKec', 'Pasien\PasienController@ajaxListKec')->name('pasien.ajaxListKec');
		Route::get('/list/ajaxListKel', 'Pasien\PasienController@ajaxListKel')->name('pasien.ajaxListKel');
		Route::post('/list/add-data', 'Pasien\PasienController@addPasien')->name('pasien.addPasien');
		Route::get('/list/ajaxRm', 'Pasien\PasienController@ajaxRm')->name('pasien.ajaxRm');
		Route::post('/list/changeStatus', 'Pasien\PasienController@changeStatus')->name('pasien.changeStatus');
		Route::post('/list/editPasien', 'Pasien\PasienController@editPasien')->name('pasien.editPasien');
		Route::get('/list/ajaxDetPasien', 'Pasien\PasienController@ajaxDetPasien')->name('pasien.ajaxDetPasien');

		Route::get('/regist/ajaxGetPasienPoli', 'Pasien\PasienController@ajaxGetPasienPoli')->name('regist.ajaxGetPasienPoli');
		Route::get('/regist/ajaxDetPasRm', 'Pasien\PasienController@ajaxDetPasRm')->name('regist.ajaxDetPasRm');
		Route::get('/regist/getDok', 'Pasien\PasienController@getDok')->name('regist.getDok');
		Route::post('/regist/addPendaftaran', 'Pasien\PasienController@addPendaftaran')->name('regist.addPendaftaran');
		Route::get('/regist/printPeriksa', 'Pasien\PasienController@printPeriksa')->name('regist.printPeriksa');

		Route::get('/kasir/print-nota', 'Layanan\KasirController@printNota')->name('layanan.kasir.printNota');
		
		
	});

	Route::prefix('/master')->middleware(['permission:menu-master-data'])->group(function(){
		Route::get('/kecamatan', 'Master\AlamatController@kecamatan')->name('master.alamat.kecamatan');
		Route::get('/kecamatan/ajaxGetKec', 'Master\AlamatController@ajaxGetKec')->name('master.alamat.ajaxGetKec');
		Route::post('/kecamatan/addKec', 'Master\AlamatController@addKec')->name('master.alamat.addKec');
		Route::post('/kecamatan/editKec', 'Master\AlamatController@editKec')->name('master.alamat.editKec');
		Route::post('/kecamatan/delKec', 'Master\AlamatController@delKec')->name('master.alamat.delKec');

		Route::get('/kelurahan', 'Master\AlamatController@kelurahan')->name('master.alamat.kelurahan');
		Route::get('/kelurahan/ajaxListKec', 'Master\AlamatController@kelurahan')->name('master.alamat.ajaxListKec');
		Route::get('/kelurahan/ajaxGetKel', 'Master\AlamatController@ajaxGetKel')->name('master.alamat.ajaxGetKel');
		Route::post('/kelurahan/addKel', 'Master\AlamatController@addKel')->name('master.alamat.addKel');
		Route::post('/kelurahan/editKel', 'Master\AlamatController@editKel')->name('master.alamat.editKel');
		Route::post('/kelurahan/delKel', 'Master\AlamatController@delKel')->name('master.alamat.delKel');
		Route::get('/kelurahan/ajaxGetCombkec', 'Master\AlamatController@ajaxGetCombkec')->name('master.alamat.ajaxGetCombkec');


		Route::get('/poli', 'Master\DokterController@poli')->name('master.dokter.poli');
		Route::get('/poli/ajaxListPoli', 'Master\DokterController@ajaxListPoli')->name('master.dokter.ajaxListPoli');
		Route::post('/poli/addPoli', 'Master\DokterController@addPoli')->name('master.dokter.addPoli');

		Route::get('/dokter', 'Master\DokterController@dokter')->name('master.dokter.dokter');
		Route::get('/dokter/ajaxListDokter', 'Master\DokterController@ajaxListDokter')->name('master.dokter.ajaxListDokter');
		Route::get('/dokter/ajaxGetPoli', 'Master\DokterController@ajaxGetPoli')->name('master.dokter.ajaxGetPoli');
		Route::get('/dokter/delPoli', 'Master\DokterController@delPoli')->name('master.dokter.delPoli');
		Route::post('/dokter/editPoli', 'Master\DokterController@editPoli')->name('master.dokter.editPoli');
		Route::post('/dokter/addDokter', 'Master\DokterController@addDokter')->name('master.dokter.addDokter');
		Route::get('/dokter/editDokter', 'Master\DokterController@editDokter')->name('master.dokter.editDokter');
		Route::get('/dokter/deleteDokter', 'Master\DokterController@deleteDokter')->name('master.dokter.deleteDokter');

		Route::get('/katg-satuan', 'Master\ProductController@kategSatuan')->name('master.product.kategSatuan');
		Route::get('/katg-satuan/ajaxGetSatuan', 'Master\ProductController@ajaxGetSatuan')->name('master.product.ajaxGetSatuan');
		Route::post('/katg-satuan/addSatuan', 'Master\ProductController@addSatuan')->name('master.product.addSatuan');
		Route::get('/katg-satuan/editSatuan', 'Master\ProductController@editSatuan')->name('master.product.editSatuan');
		Route::get('/katg-satuan/deleteSatuan', 'Master\ProductController@deleteSatuan')->name('master.product.deleteSatuan');

		Route::get('/katg-satuan/ajaxGetKategory', 'Master\ProductController@ajaxGetKategory')->name('master.product.ajaxGetKategory');
		Route::post('/katg-satuan/addKategory', 'Master\ProductController@addKategory')->name('master.product.addKategory');
		Route::get('/katg-satuan/editKategory', 'Master\ProductController@editKategory')->name('master.product.editKategory');
		Route::get('/katg-satuan/deleteKategory', 'Master\ProductController@deleteKategory')->name('master.product.deleteKategory');
		
		Route::get('/product/product', 'Master\ProductController@product')->name('master.product.product');
		Route::get('/product/ajaxGetProduct', 'Master\ProductController@ajaxGetProduct')->name('master.product.ajaxGetProduct');
		Route::get('/product/ajaxListKatgSat', 'Master\ProductController@ajaxListKatgSat')->name('master.product.ajaxListKatgSat');
		Route::post('/product/addProduct', 'Master\ProductController@addProduct')->name('master.product.addProduct');
		Route::get('/product/getProduct', 'Master\ProductController@getProduct')->name('master.product.getProduct');
		Route::post('/product/getProduct', 'Master\ProductController@editProduct')->name('master.product.editProduct');
		Route::post('/product/delProd', 'Master\ProductController@delProd')->name('master.product.delProd');
	});

	Route::prefix('/pelayanan')->middleware(['permission:menu-pelayanan-pasien'])->group(function(){
		Route::get('/resep', 'Layanan\ResepController@resep')->name('layanan.resep');
		Route::get('/resep/ajaxGetPeriksa', 'Layanan\ResepController@ajaxGetPeriksa')->name('layanan.resep.ajaxGetPeriksa');
		Route::get('/resep/getProduct', 'Layanan\ResepController@getProduct')->name('layanan.resep.getProduct');
		Route::post('/resep/addTransaksi', 'Layanan\ResepController@addTransaksi')->name('layanan.resep.addTransaksi');

		Route::get('/kasir', 'Layanan\KasirController@kasir')->name('layanan.kasir');
		Route::get('/kasir/ajaxGetPeriksa', 'Layanan\KasirController@ajaxGetPeriksa')->name('layanan.kasir.ajaxGetPeriksa');
		Route::get('/kasir/getResep', 'Layanan\KasirController@getResep')->name('layanan.kasir.getResep');
		Route::get('/kasir/listBea', 'Layanan\KasirController@listBea')->name('layanan.kasir.listBea');
		Route::post('/kasir/addBeaPeriksa', 'Layanan\KasirController@addBeaPeriksa')->name('layanan.kasir.addBeaPeriksa');
		Route::post('/kasir/addPembayaran', 'Layanan\KasirController@addPembayaran')->name('layanan.kasir.addPembayaran');
		Route::get('/kasir/print-nota', 'Layanan\KasirController@printNota')->name('layanan.kasir.printNota');
		Route::post('/kasir/deleteItem', 'Layanan\KasirController@deleteItem')->name('layanan.kasir.deleteItem');

		Route::get('/periksa', 'Layanan\PeriksaController@periksa')->name('layanan.periksa');
		Route::get('/periksa/ajaxPeriksa', 'Layanan\PeriksaController@ajaxPeriksa')->name('layanan.periksa.ajaxPeriksa');
		Route::get('/periksa/ajaxRiwayat', 'Layanan\PeriksaController@ajaxRiwayat')->name('layanan.periksa.ajaxRiwayat');
		Route::post('/periksa/addPeriksa', 'Layanan\PeriksaController@addPeriksa')->name('layanan.periksa.addPeriksa');
		Route::get('/periksa/ajaxListDokter', 'Layanan\PeriksaController@ajaxListDokter')->name('layanan.periksa.ajaxListDokter');


		
	});
	

	Route::prefix('/report')->middleware(['permission:menu-report'])->group(function(){
		
		Route::get('/transaksi', 'Report\ReportController@reportTransaksi')->name('report.transaksi.reportTransaksi');
		Route::get('/transaksi/ajaxGetTrans', 'Report\ReportController@ajaxGetTrans')->name('report.transaksi.ajaxGetTrans');
		Route::get('/transaksi/exportTrans', 'Report\ReportController@exportTrans')->name('report.transaksi.exportTrans');


		
	});


});