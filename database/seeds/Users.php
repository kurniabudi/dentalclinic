<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use Carbon\Carbon;
class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles  = Role::Select('id')->get();

        $user = new User();
        $user->name = 'Admin ICT BBI';
        $user->nik = 'admin';
        $user->email = 'admin@klinik.co.id';
        $user->email_verified_at = carbon::now();
        $user->password =  bcrypt('Pass@123');
        $user->state = 'super_admin';

        if($user->save()){
            $user->attachRoles($roles);   
        }
    }
}
